## Class Assignment 4 Report ##
___

    Márcia Guedes [1201771@isep.ipp.pt] | may 2021
___

## Containers ##
Like virtual machines, containers are computer environments that contain several IT components, such as hardware, programs, applications, databases, among others, that isolate them from system rest. In other words, a container is a standard unit of software that packages the code and all its dependencies so that the application can be built quickly and reliably from one base environment to another.

In other words,  virtual machines are an abstraction of physical hardware, allowing to turn one server into many. The hypervisor allows several virtual machines to operate on a single computer, and each one of these virtual machines contains a full copy of an operating system, the application, necessary binaries, and libraries. In comparison, containers are an abstraction at the app layer that packages code and dependencies together. Several containers can also run on a single machine, but all of them share the same OS kernel (they don't have a full copy of the operating system), even though their process runs independently. A container tends to behave like micro-services, that is, it is often used to package individual functions that perform very specific tasks. 

The primary features that differentiate virtual machines from containers are scale and portability.  As explained above, virtual machines contain their operation system, which makes them measured in gigabytes. Containers are lighter (measured in megabytes) since they only contain applications and the necessary files to run them.

Regarding the second characteristic, since containers share an operating system, the migration between processes is an easier process. Thus, containers are considered widely environments to deploy native applications in modern clouds, which enables a consistent development and automated management of the micro-service (allowing an acceleration of the optimization of the apps).

However, containers present some disadvantages when compared with virtual machines, being the compatibility with the underlying operating system the main one. The second one is the ability for virtual machines to perform much more operations than containers. Both these characteristics, until today, make virtual machines still the best operational method, even though their portability is more limited.

___

## Docker ##
If a container is an isolated environment, a docker is an open-source platform in the Go programming language, with high performance. Docker is composed of pieces of software from a complete file system that includes all the resources necessary for its execution. It is an implementation of container virtualization.

Docker container technology was released in 2013 as an open-source Docker Engine, that adapted existing computing concepts around containers. Docker technology focuses on the requirements of system developers and operators to separate application dependencies from the infrastructure. The technology available from Docker and its open-source project has been leveraged by all major data center vendors and cloud providers.

One of these main advantages is that Docker containers share the machine's operating system kernel and therefore do not require one operating system per application, driving greater server efficiency and reducing server and licensing costs.

Docker, and other container's tolls, provide an image-based deployment model, which is a simple way to hare an application or set of services, including all their dependencies across multiple environments. 

A Docker container image is a lightweight, self-contained, executable software package that contains all the necessary information to run an application, such as, code, system variables, settings,... 

When Docker images are run on Docker Engines, they became Docker containers, regardless of the operating system. These containers, protect the software from its environment and ensure that it works smoothly despite any differences.

Usually, Linux containers are prepared to run multiple processes, which means that entire applications run as one. However, Docker technology encourages applications to be segregated into separate processes, by providing tools for those granular approaches.

In an overall picture, Docker containers main advantages are:

    * modularity: ability to disable a part of an application, either for repair or update, without stopping it entirely.
    * layers and image version: each docker image is formed through a series of layers. When the user specifies a command a new layer is created. Docker reuses these layers to build new containers, which makes the build process much faster.
    * reverse: ability to reverse when necessary, which is possible due to the docker image being created on layers.
    * fast deployment: since it is not necessary to boot an operating system to add or move a container, deployment time is substantially less

#### Install and Set up #####

To install the docker, the user must access the link: [https://docs.docker.com/docker-for-windows/install/]. During this process it is necessary to be careful, namely restarting the computer. When installing, an image like the one shown below should appear. Pop-up tests should remain untouched until further information (as described in the image below). That is, the user should not restart immediately, only later. 

![image1](docker_img1.PNG)

To work with the docker there are two possible ways, which depend on the properties and version of the windows operating system used by the user, in which all information is available at the link: [https://docs.docker.com/docker-for-windows/install/].

In a simplified way, depending on the Windows operating system version, the docker will behave differently. In short, Docker will use additional software to run the backend processes. If the user has Hyper-V installed on their local system then Docker will use it to run the containers. If it doesn't have Hyper-V, the user must install Windows Subsystem for Linux and Ubuntu software. More detailed explanatory information can be found at the following link: [https://docs.microsoft.com/en-us/dotnet/architecture/microservices/container-docker-introduction/docker-defined].

After these settings, it is now necessary for the user to restart the computer. After that, the Docker should already be operational.

![image2](docker_img2.PNG)

To verify that the entire process was operational, it is now possible to run the command below, which provides all the information regarding the local containers.

        $ docker info
        
        (e.g.)
        C:\WINDOWS\system32> docker info
        Client:
        Context:    default
        Debug Mode: false
        Plugins:
        app: Docker App (Docker Inc., v0.9.1-beta3)
        buildx: Build with BuildKit (Docker Inc., v0.5.1-docker)
        compose: Docker Compose (Docker Inc., 2.0.0-beta.1)
        scan: Docker Scan (Docker Inc., v0.6.0)

        Server:
        Containers: 0
        Running: 0
        Paused: 0
        Stopped: 0
        Images: 0
        Server Version: 20.10.6
        Storage Driver: overlay2
        Backing Filesystem: extfs
        Supports d_type: true
        Native Overlay Diff: true
        userxattr: false
        Logging Driver: json-file
        Cgroup Driver: cgroupfs
        Cgroup Version: 1
        Plugins:
        Volume: local
        Network: bridge host ipvlan macvlan null overlay
        Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
        Swarm: inactive
        Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
        Default Runtime: runc
        Init Binary: docker-init
        containerd version: 05f951a3781f4f2c1911b05e61c160e9c30eaa8e
        runc version: 12644e614e25b05da6fd08a38ffa0cfe1903fdec
        init version: de40ad0
        Security Options:
        seccomp
        Profile: default
        Kernel Version: 5.4.72-microsoft-standard-WSL2
        Operating System: Docker Desktop
        OSType: linux
        Architecture: x86_64
        CPUs: 8
        Total Memory: 12.31GiB
        Name: docker-desktop
        ID: 5VFC:EQVQ:QXAX:J4CQ:UTXU:YOX5:RX2S:IHRB:VVUM:3OMX:AWHY:IZE2
        Docker Root Dir: /var/lib/docker
        Debug Mode: false
        Registry: https://index.docker.io/v1/
        Labels:
        Experimental: false
        Insecure Registries:
        127.0.0.0/8
        Live Restore Enabled: false

        WARNING: No blkio throttle.read_bps_device support
        WARNING: No blkio throttle.write_bps_device support
        WARNING: No blkio throttle.read_iops_device support
        WARNING: No blkio throttle.write_iops_device support

Based on the information provided above, it appears that at the moment there is no container or image created (Containers: 0).

##### Working with Docker #####

A Docker image is a read-only template that contains a set of instructions for creating a container that can run on the Docker platform. It provides a convenient way to package pre-configured server environments and applications, which you can use for your private use or share publicly with other Docker users. Docker images are also the starting point for anyone using Docker for the first time, as explained above.

There are two different ways to use Docker Images:

    * Interactive: running a container from an existing Docker image, manually changing that container environment through a series of live steps, and saving the resulting state as a new image.
    * Dockerfile: building a simple text file, known as a Dockerfile, which provides the specifications for creating a Docker image.

###### Create a container a partir de uma imagem de um repositório ######
In the first phase, it is intended that the user creates a container from an image that already exists in a repository. For the user to create a container, it must run the command below.
        
        docker run -d -p 80:80 docker/getting-started

        (e.g.)
        [...]\DevOps\docker_v.1> docker run -d -p 80:80 docker/getting-started
        Unable to find image 'docker/getting-started:latest' locally
        latest: Pulling from docker/getting-started
        ba3557a56b15: Pull complete
        468d8ccebf7a: Pull complete
        b7f67c5d6ce9: Pull complete
        ed91f01a4fcb: Pull complete
        8051568c89ac: Pull complete
        5b4dcb4d3646: Pull complete
        22ecfaabb4de: Pull complete
        bbaa3642d0a3: Pull complete
        Digest: sha256:67944b53f8a7d16b3d9909315f9d557ade12c4ee4083cd98068f9fe6d9995808
        Status: Downloaded newer image for docker/getting-started:latest
        63ab4c0414969ca5ce5af3edc7bcd70b5106f1d8af259d1bee18250e4cb7a11e

![image4](docker_img4.PNG)

###### Check containers & images status ######
To visualize the created container, the user can run the command below, where some information about that container is also available, namely the name and its id. Note that this command only allows the user to view containers that are in operation (connected).

        $ docker ps
        (e.g.)
        [...]\DevOps\docker_v.1>docker ps
        CONTAINER ID   IMAGE                    COMMAND                  CREATED         STATUS         PORTS                               NAMES
        63ab4c041496   docker/getting-started   "/docker-entrypoint.…"   3 minutes ago   Up 3 minutes   0.0.0.0:80->80/tcp, :::80->80/tcp   cool_kowalevski

Since no image was made available, the creation of this container is done from an image made available by the Docker repository, as verified in the container information. In this case, the docker/getting-started image is obtained from this repository (same name).

To access information about the image downloaded and used to create the docker, use the command below. This command can return information about all images present on the local system. In this case, it only contains the one downloaded from the repository.

        $ docker images
        (e.g.)
        [...]\DevOps\docker_v.1>docker images
        REPOSITORY                    TAG               IMAGE ID       CREATED        SIZE
        docker/getting-started        latest            3ba8f2ff0727   2 months ago   27.9MB

After the container is created, it is possible to view it in the Docker interface, as shown in the image below.

![image5](docker_img5.PNG)

Note that when a container is created, by definition, it is automatically initialized, and the project is running. Throught thr url 'http:localhost:8080/basic-0.0.1-SNAPSHOT' it is possible access the application.

![image6](docker_img6.PNG)

IF the user wants to access the database, the url available is: 'http:localhost:8080/basic-0.0.1-SNAPSHOT/h2-console' and the following image should appear.

![image7](docker_img7.PNG)

To enter, the user only needs to put 'JBDC URL' the following line, also shown above. And then, it is possible to access the database.

     jdbc:h2:tcp://192.168.33.11:9092/./jpadb

![image8](docker_img8.PNG)

###### Stop a container ######

To stop a container, the user must run the command below. Note that this command only stops the container, it does not delete it.

        $ docker stop <container_name>
        or
        $ docker stop <container_ID>
        (e.g)
        C:\WINDOWS\system32>docker stop cool_kowalevski
        cool_kowalevski

###### Start a container ######
To restart the container again, the user only needs to run the command below.

        $ docker start <container_name>
        or
        $ docker start <container_ID>
        (e.g)
        C:\WINDOWS\system32>docker start cool_kowalevski
        cool_kowalevski        

###### Remove containers & images ######
To delete a container, the user can use the command below. This command is only possible to be executed if the container is stopped.

        $ docker rm <container_name>
        or
        $ docker rm <container_ID>
        C:\WINDOWS\system32>docker rm cool_kowalevski
        cool_kowalevski


An important note is that this command deletes the container but not the images associated with it, as shown in the example below.

        $ docker images
        (e.g.)
        C:\WINDOWS\system32> docker images
        REPOSITORY                    TAG               IMAGE ID       CREATED        SIZE
        docker/getting-started        latest            3ba8f2ff0727   2 months ago   27.9MB

To remove the images, the user needs to run the following command.

        $ docker rmi <image_ID>
        (e.g.)
        C:\WINDOWS\system32>docker rmi 3ba8f2ff0727
        Untagged: docker/getting-started:latest
        Untagged: docker/getting-started@sha256:67944b53f8a7d16b3d9909315f9d557ade12c4ee4083cd98068f9fe6d9995808
        Deleted: sha256:3ba8f2ff0727d36bccad59f6344f7f3a101bcd9c70b984aa2dd006914c496cb8
        Deleted: sha256:1b69d40f20c016df9bf808ddb3c838f6fc414f31dd5aa6bb8c750c007de7bb16
        Deleted: sha256:67a715e7ea25af1cbdca2e453b06c5ff218750ec2a84d0f1b9508f8c2053a7b1
        Deleted: sha256:22dd1c631d282d9c89a77fb4592d8f32dcc4e946228cf0b671a2d37c2511e1dc
        Deleted: sha256:3c18bd279327e455da410b83dc432bc05de8ef76e7ec6fb0ad0f9129ec497a7e
        Deleted: sha256:f5df52d1ff06d00bbd6f1c9ec68a08a83af4cd9a6f238248bb49182d1519eeaf
        Deleted: sha256:531c1d9434cbc735dff74ae43218e1298308f42380f6a495bc36d59556bc7ff9
        Deleted: sha256:78989d927da75e536674888b072a8273144f46347b2c7f527d8956d4e8ecd591
        Deleted: sha256:cb381a32b2296e4eb5af3f84092a2e6685e88adbc54ee0768a1a1010ce6376c7

###### Create a container a partir de um dockerfile ######

As explained above, it is also possible to create a container from docker files. The advantage of a Dockerfile over just storing the binary image (or a snapshot/template on other virtualization systems) is that automatic builds will ensure the user has the latest version available. This is good from a security standpoint, as the user wants to ensure that you are not installing any vulnerable software.

The construction of a docker file must follow certain rules. Help information is available at the link [https://thenewstack.io/docker-basics-how-to-use-dockerfiles/].


A typical Dockerfile example is available in the following repository: [ https://bitbucket.org/atb/docker-compose-spring-tut-demo.git]. In this example, the user has two example images, as we have a Dockerfile for a web image and a Dockerfile for a database (db) image. The user can clone the repository and get the Dockerfile examples. Do not clone the repository inside the user's own repository.

The first file the user needs to copy is the Docker-compose file, which should present the following structure.

    version: '3'
    services:
      web:
        build: web
        ports:
          - "8080:8080"
        networks:
          default:
            ipv4_address: 192.168.33.10
        depends_on:
          - "db"
      db:
        build: db
        ports:
          - "8082:8082"
          - "9092:9092"
        volumes:
          - ./data:/usr/src/data
        networks:
          default:
            ipv4_address: 192.168.33.11
    networks:
      default:
        ipam:
          driver: default
          config:
            - subnet: 192.168.33.0/24

The second file that the user needs to copy is the web Dockerfile. For that, the user should create a directory named 'web' and inside add a Dockerfile with the following structure.

    FROM tomcat

    RUN apt-get update -y

    RUN apt-get install -f

    RUN apt-get install git -y

    RUN apt-get install nodejs -y

    RUN apt-get install npm -y

    RUN mkdir -p /tmp/build

    WORKDIR /tmp/build/

    RUN git clone https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git

    WORKDIR /tmp/build/devops-20-21-1201771/ca3/part2/gradle/tut-basic

    RUN chmod u+x gradlew

    RUN ./gradlew clean build

    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8080

As can be seen above, the user should put his own git repository and the correct paths through the project, as the war file's name (nameFile.war)

Another important aspect is that, if the user runs in the windows operating system, it is necessary to add the script to the following command, which will give access to run the gradlew file. It is also shown in the previous image. 

     RUN chmod u+x gradlew

Finally, the user needs to repeat the process for the database Dokerfile, which consists in create a directory called 'db' and inside store another Dockerfile with the following structure. In this case, no changes are requeired.

    FROM ubuntu

    RUN apt-get update && \
      apt-get install -y openjdk-8-jdk-headless && \
      apt-get install unzip -y && \
      apt-get install wget -y

    RUN mkdir -p /usr/src/app

    WORKDIR /usr/src/app/

    RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

    EXPOSE 8082
    EXPOSE 9092

    CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

Now, to start the application, the user only needs to access the directory that contains the docker-compose file and run the following command.

    $ docker-compose up

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\docker> docker-compose up

    Creating network "docker_default" with the default driver

    ERROR: Pool overlaps with other one on this address space

As can be seen above, sometimes the command can fail, showing the previous information. If that happens, the user needs to go through the following process. First, the user needs to check the available docker networks, through the command below.

    $ docker network ls

    (e.g)
    [...]\DevOps\devops-20-21-1201771\ca4\docker>docker network ls
    NETWORK ID     NAME                                     DRIVER    SCOPE
    0504a2f7d0fa   bridge                                   bridge    local
    d231150abbed   docker-compose-spring-tut-demo_default   bridge    local
    035c836d0725   host                                     host      local
    bb6b66448cca   none                                     null      local

Then, the user needs to remove the network that is being used. 

    $ docker network rm <network_ID>

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\docker>docker network rm d231150abbed
    d231150abbed

And now, run the previous command again, to iniciate docker compose file.

    [...]\DevOps\devops-20-21-1201771\ca4\docker>docker-compose up
    Docker Compose is now in the Docker CLI, try `docker compose up`

    Creating network "docker_default" with the default driver
    Building db
    [+] Building 1.6s (9/9) FINISHED
     => [internal] load build definition from Dockerfile                                                                                                                   0.1s
     => => transferring dockerfile: 463B                                                                                                                                   0.0s
     => [internal] load .dockerignore             

Accessign the url 'http:localhost:8080/demo-0.0.1-SNAPSHOT' it can again access to  the new application, or the database (url: 'http:localhost:8080/demo-0.0.1-SNAPSHOT/h2-console')

![image9](docker_img9.PNG)

![image10](docker_img10.PNG)

Again, without forgetting to put the 'JBDC URL' variable with the following path.

     jdbc:h2:tcp://192.168.33.11:9092/./jpadb

###### Stop and Restart Containers ######

If the user wants to stop the containers, only needs to run the follwong command, and both db and web containers will stop.

    $ docker-compose stop 
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\docker>docker-compose stop
    Stopping docker_web_1 ... done
    Stopping docker_db_1  ... done

And to restart them both again, the command below.

    $ docker-compose start
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\docke>docker-compose start
    Starting db  ... done
    Starting web ... done

However, if the user only wants to stop or start one of the containers, it needs to run the following command, respectively.

    $ docker-compose stop <container_name>
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\docke>docker-compose stop web
    Stopping docker_web_1 ... done


    $ docker-compose start <container_name>
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\docke>docker-compose start web
    Starting web ... done


##### Create a DockerHub #####

To store docker images or any container images, the user can access Docker Hub and create a repository. More information and sign up process are available at the following link: [https://hub.docker.com/]

![image12](docker_img12.PNG)

##### Push Images to DockerHub #####

To push an image to the Docker Hub, the user only needs to use the following commands. More info available in [https://docs.docker.com/docker-hub/repos/].

    $ docker tag <existing-image> <hub-user>/<repo-name>[:<tag>]
    (e.g.)
    $ docker tag web_image

##### Copy of the database file. #####

Finally, if the user wants a copy of the database file, the user needs to run the command below.

    $ docker exec -it <container_name> /bin/sh -c "cp *.db ../data/"
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\docke>docker exec -it docker_db_1 /bin/sh -c "cp *.db ../data/"

A directory should appear in the file's system on the project, called 'data', where the information is now stored.

![image13](docker_img13.PNG)

___

## Kubernetes ##

Kubernetes is an open-source container orchestrator that provides a comprehensive system for automating deployment, scheduling, and scaling of containerized applications, and supports many containerization tools such as Docker.

It is the market leader and the standardized means of orchestrating containers and deploying distributed applications, and when running on a public cloud service or on-premises, it is highly modular.

Kubernetes and Docker are both comprehensive de-facto solutions to intelligently manage containerized applications and provide powerful capabilities. However, these tools comprehend different roles and applications.

As explained before, Docker is a platform and tool for building, distributing and running processes through Docker containers. However, Kubernetes is a container orchestration system for Docker containers whose purpose is to coordinate clusters of nodes at scale in production in an efficient manner. It is possible to run a  Docker build on a Kubernetes cluster, but Kubernetes includes custom plugins. For more information on these subjects, access the following link [https://www.sumologic.com/blog/kubernetes-vs-docker/].


#### Set up Docker Containers for Kubernets #####

Same as in the Docker procedure, described before, it is also necessary to set up 3 files: the docker-compose, and the two Dokerfiles for web and database containers.

Starting with the docker-compose file, it should present a structure according to the one described below.

    version: '3'
    services:
      web:
        image: 
        ports:
          - "8080:8080"
        depends_on:
          - "db"
      db:
        image: 

For now, the images designated above are empty. They will be filled in later during the process.

In the next step, as same as before, the user should create a directory called 'web' where it should save a Dockerfile to create the 'web' container, with the same structure as describe below. Not forgetting to put the correct paths of the project directories.

    FROM tomcat

    RUN apt-get update -y

    RUN apt-get install -f

    RUN apt-get install git -y

    RUN apt-get install nodejs -y

    RUN apt-get install npm -y

    RUN mkdir -p /tmp/build

    WORKDIR /tmp/build/

    RUN git clone https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git

    WORKDIR /tmp/build/devops-20-21-1201771/ca4/gradle/tut-basic

    RUN chmod u+x gradlew

    RUN ./gradlew clean build

    RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8080

And the same for the database container 'db', with the following structure.

    FROM ubuntu

    RUN apt-get update && \
      apt-get install -y openjdk-8-jdk-headless && \
      apt-get install unzip -y && \
      apt-get install wget -y

    RUN mkdir -p /usr/src/app

    WORKDIR /usr/src/app/

    RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

    EXPOSE 8082
    EXPOSE 9092

    CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

Finally, now regarding the project to which the user wants to use for these processes, it is necessary to change the application.properties file and change the following configuration.

    spring.datasource.url=jdbc:h2://db:9092

After all the files are configurated, the user needs to create the docker images for the 'web' and 'db' containers. For that, the user only needs to access the directory where each Dockerfile is stored and run the following command.

    $ docker-compose build --no-cache <image_name>

    (e.g. : database container)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>docker-compose build --no-cache db
    Building db
    [+] Building 72.0s (10/10) FINISHED
     => [internal] load build definition from Dockerfile 
     => => transferring dockerfile: 463B                                                    
     => [internal] load .dockerignore
     => => transferring context: 2B   
     => [internal] load metadata for docker.io/library/ubuntu:latest
     => [auth] library/ubuntu:pull token for registry-1.docker.io                                                      
     => CACHED [1/5] FROM docker.io/library/ubuntu@sha256:cf31af331f38d1d7158470e095b132acd126a7180a54f263d386da88eb681d93 
     => [2/5] RUN apt-get update &&   apt-get install -y openjdk-8-jdk-headless &&   apt-get install unzip -y &&   apt-get install wget -y           
     => [3/5] RUN mkdir -p /usr/src/app                  
     => [4/5] WORKDIR /usr/src/app/                                                    
     => [5/5] RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar                                                     
     => exporting to image                       
     => => exporting layers    
     => => writing image sha256:2286f511d0c30e498fb018b9c3ed262381513b7b3927b43f803bc5d318e51396   
     => => naming to docker.io/library/alternativa_db 

     (e.g. : web container)
     [...]\DevOps\devops-20-21-1201771\ca4\alternativa>docker-compose build --no-cache web
     Building web
    [+] Building 149.8s (18/18) FINISHED
      => [internal] load build definition from Dockerfile                                              
      => => transferring dockerfile:535B                                         
      => [internal] load .dockerignore                                            
      => => transferring context: 2B                                                      
      => [internal] load metadata for docker.io/library/tomcat:latest                                                               
      => [auth] library/tomcat:pull token for registry-1.docker.io                                                                         
      => CACHED [ 1/13] FROM docker.io/library/tomcat@sha256:10842dab06b5e52233ad977d4689522d4fbaa9c21e6df387d7a530e02316fb45                                     
      => [ 2/13] RUN apt-get update -y                                                                         
      => [ 3/13] RUN apt-get install -f    
      => [ 4/13] RUN apt-get install git -y 
      => [ 5/13] RUN apt-get install nodejs -y 
      => [ 6/13] RUN apt-get install npm -y
      => [ 7/13] RUN mkdir -p /tmp/build 
      => [ 8/13] WORKDIR /tmp/build/ 
      => [ 9/13] RUN git clone https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git 
      => [10/13] WORKDIR /tmp/build/devops-20-21-1201771/ca4/gradle/tut-basic 
      => [11/13] RUN chmod u+x gradlew 
      => [12/13] RUN ./gradlew clean build 
      => [13/13] RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ 
      => exporting to image                                                                      
      => => exporting layers
      => => writing image sha256:f56521dce8da47a847fd2778e4568bff3372529511586746e31fbef565379f25  
      => => naming to docker.io/library/alternativa_web

To check if both docker images were created, the user can run the following command.

    $ docker images

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>docker images
    REPOSITORY        TAG       IMAGE ID       CREATED              SIZE
    alternativa_db    latest    2286f511d0c3   About a minute ago   294MB
    alternativa_web   latest    f56521dce8da   12 minutes ago       1.84GB

After both images are created, the user needs to push both of them to a Doker Hub repository, thought the following commands - where it tags the docker image and then push them to the repository.

    docker tag <nome_imagem> <user/nome_repos>:<nome da tag>

    (e.g.: db container)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>docker tag alternativa_db 1201771/devops_2021_1201771:alternative_db

    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>docker push 1201771/devops_2021_1201771:alternative_db
    The push refers to repository [docker.io/1201771/devops_2021_1201771]
    a3a70506aa39: Pushed
    5f70bf18a086: Layer already exists
    22aa263175fd: Pushed
    25c7a54757d2: Pushed
    2f140462f3bc: Layer already exists
    63c99163f472: Layer already exists
    ccdbb80308cc: Layer already exists
    alternative_db: digest: sha256:3350d476da3f76904d1415f9a5d0bb52fd2fdb123b2e32f58f1a35843c7af924 size: 1779

    (e.g.: web container)

    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>docker tag alternativa_web 1201771/devops_2021_1201771:alternative_web

    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>docker push 1201771/devops_2021_1201771:alternative_web
    The push refers to repository [docker.io/1201771/devops_2021_1201771]
    a004b2832bcb: Pushed
    2971d00b5a04: Pushed
    4d951e12f440: Pushed
    5f70bf18a086: Layer already exists
    89d922756198: Pushed
    dff06cfb9fc9: Pushed
    bc59b59e1f8c: Pushed
    f71d464293ae: Pushed
    3170774ecf15: Pushed
    56c61e3c63af: Pushed
    dd9b9aaa00a7: Pushed
    daf63ef0ddbb: Layer already exists
    3307ffa538c1: Layer already exists
    8f8b5acac684: Layer already exists
    15786a1cf1cb: Layer already exists
    6f770cdc9ebf: Layer already exists
    3fc095fab4a2: Layer already exists
    685934357c89: Layer already exists
    ccb9b68523fd: Layer already exists
    00bcea93703b: Layer already exists
    688e187d6c79: Layer already exists
    alternative_web: digest: sha256:15510cafbf92b2fa1e338e1a6270904a2451127b6c2ba27abce1bb2eb8100de9 size: 4936

![image1](kubernets_img1.PNG)

#### Work with Kubernets #####

Now that the main initial set up is complete, the user needs to install a few softwares in order to work with Kubernets. To start, it is necessary to install three aditional softwares: Kompose, Minikube and Kubectl. 

##### Install Kubectl #####
To start, the user can install Kubectl, where more information are available in the following link [https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/#install-kubectl-binary-with-curl-on-windows]. For that, the user runs the command below in git bash.

    $ curl -LO https://dl.k8s.io/release/v1.21.0/bin/windows/amd64/kubectl.exe 

##### Install Kompose #####
The second software to install is Kompose. In this case, the user needs to have a little more careful in this process. This software should be install in the same package where the docker-compose file is stored, though the command below. More information available in the link above.

    $ curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-windows-amd64.exe -o kompose.exe

After that, the user needs to give permition to execute this program though the command below.

    $ chmod +x kompose

##### Install Minikube #####
Finally, the user needs to install Minikube, where all the information are available in the link: [https://minikube.sigs.k8s.io/docs/start/#debian-package]


#### Final setups  #####
Now that all the necessary additional software is installed, it is necessary to set up again the docker-compose file. As explained before, this file is not yet completed, since the images were not inserted yet. Thus, the user needs now to access the Docker Hub repository to where the docker images were previously pushed and access to its information.

![image2](kubernets_img2.PNG)

![image3](kubernets_img3.PNG)

Then, the user needs to copy each of these docker images' names and add them to the docker-compose file, as represented below.

    version: '3'
    services:
      web:
        image: 1201771/devops_2021_1201771:alternative_web
        ports:
          - "8080:8080"
        depends_on:
          - "db"
      db:
        image: 1201771/devops_2021_1201771:alternative_db

Now that all the setups are done, on a new terminal (bash or prompt), the user needs to start the minikube softwre, as described in the following command.

    $ minikube start

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>minikube start
    * minikube v1.20.0 on Microsoft Windows 10 Pro 10.0.19042 Build 19042
    * Automatically selected the docker driver. Other choices: hyperv, virtualbox, ssh
    * Starting control plane node minikube in cluster minikube
    * Pulling base image ...
    * Downloading Kubernetes v1.20.2 preload ...
        > preloaded-images-k8s-v10-v1...: 491.71 MiB / 491.71 MiB  100.00% 21.57 Mi
        > gcr.io/k8s-minikube/kicbase...: 358.10 MiB / 358.10 MiB  100.00% 10.95 Mi
        > gcr.io/k8s-minikube/kicbase...: 358.10 MiB / 358.10 MiB  100.00% 9.26 MiB
    * Creating docker container (CPUs=2, Memory=4000MB) ...
    * Preparing Kubernetes v1.20.2 on Docker 20.10.6 ...
      - Generating certificates and keys ...
      - Booting up control plane ...
      - Configuring RBAC rules ...
    * Verifying Kubernetes components...
      - Using image gcr.io/k8s-minikube/storage-provisioner:v5
    * Enabled addons: storage-provisioner, default-storageclass
    * Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default

Now, on another terminal, the user needs to create 3 files:
    * we-service.yaml
    * db-deployment.yaml
    * web-deployment.yaml

For that, the user needs to run the following command.

    $ kompose convert

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>kompose convert
    ←[36mINFO←[0m Kubernetes file "web-service.yaml" created
    ←[36mINFO←[0m Kubernetes file "db-deployment.yaml" created
    ←[36mINFO←[0m Kubernetes file "web-deployment.yaml" created

And then, the user needs to deploy those files through the next command.

    $ kubectl apply -f <fileName>

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>kubectl apply -f web-service.yaml,db-deployment.yaml,web-deployment.yaml
    service/web created
    deployment.apps/db created
    deployment.apps/web created

Finally, the user can run the command below to open the kubernets dashboard and see the status of the proccess.

    $ minikube dashboard

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>minikube dashboard
    * Enabling dashboard ...
      - Using image kubernetesui/dashboard:v2.1.0
      - Using image kubernetesui/metrics-scraper:v1.0.4
    * Verifying dashboard health ...
    * Launching proxy ...
    * Verifying proxy health ...
    * Opening http://127.0.0.1:56648/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...

![image4](kubernets_img4.PNG)

![image5](kubernets_img5.PNG)

![image6](kubernets_img6.PNG)


To finish, the user needs to open another new terminal and run the following command, which will allow running the project in port 8080 of the localhost. The pods name is available in the dashboard in the previous image.

    $ kubectl port-forward <pods_name> 8080:8080

    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca4\alternativa>kubectl port-forward web-7cd4cbfd68-sxjjj 8080:8080
    Forwarding from 127.0.0.1:8080 -> 8080
    Forwarding from [::1]:8080 -> 8080
    Handling connection for 8080
    Handling connection for 8080

In the end, the application will be runnning.

![image7](docker_img9.PNG)

___

## Conclusions ## 

Containers and virtual machines present very similar resource isolation and allocation benefits, however, they work differently due containers virtualize the operating system instead of hardware, which makes them more efficient, more portable, but (sometimes) less compatible.

The main characteristics and features of Docker are:

    * an open-source community that works for free to improve its technologies for all users.
    * safety
    * provides enterprise customers with the support they need for technologies that have been enhanced and strengthened

Docker technology is a more granular, controllable, and microservice-based approach that values efficiency.

Kubernetes and Docker are both fundamentally different technologies but they work very well together, and both facilitate the management and deployment of containers in a distributed architecture.

___

## References ##
    
    * https://www.redhat.com/pt-br/topics/containers/what-is-kubernetes
    * https://biztechmagazine.com/article/2020/02/how-get-started-docker-containers
    * https://www.redhat.com/pt-br/topics/containers/what-is-docker
    * https://www.redhat.com/pt-br/topics/containers/containers-vs-vms
    * https://blog.netapp.com/blogs/containers-vs-vms/
    * https://www.meupositivo.com.br/panoramapositivo/container-docker/
    * https://docs.docker.com/
    * https://jfrog.com/knowledge-base/a-beginners-guide-to-understanding-and-building-docker-images/
    * https://thenewstack.io/docker-basics-how-to-use-dockerfiles/
    * https://www.sumologic.com/blog/kubernetes-vs-docker/
