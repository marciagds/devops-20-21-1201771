package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmailTest {

    @Test
    void createEmailSuccessfully(){
        //arrange
        String inputEmail = "maria@gmail.com";
        //act
        Email email = new Email(inputEmail);
        //assert
        assertEquals("maria@gmail.com", email.getEmail());
    }

    @Test
    void createWrongEmail1(){
        //arrange
        String inputEmail = "maria";
        //act
        Email email = new Email(inputEmail);
        //assert
        assertEquals("error. Invalid email", email.getEmail());
    }

    @Test
    void createWrongEmail2(){
        //arrange
        String inputEmail = "@gmail.com";
        //act
        Email email = new Email(inputEmail);
        //assert
        assertEquals("error. Invalid email", email.getEmail());
    }

    @Test
    void createWrongEmail3(){
        //arrange
        String inputEmail = "mariagmail.com";
        //act
        Email email = new Email(inputEmail);
        //assert
        assertEquals("error. Invalid email", email.getEmail());
    }
}