# Class Assignment 3 Report ( Part 2)  #

___

    Márcia Guedes [1201771@isep.ipp.pt] | may 2021
___

## Vagrant ##
Vagrant is a build and management tool for virtual machine environments in a single workflow. It provides an easy-to-use workflow, by focusing on automation. Vagrant lowers development environment setup time, increases production parity, and makes the "works on my machine" excuse a relic of the past.
Vagrant main characteristics are:

    * easy configuration
    * easy reproduction
    * portable work environments built on top of industry-standard technology

To install Vagrant, the user needs to access the following link[https://www.vagrantup.com/downloads] and download the content. To check if vagrant was correctly installed, the user can run the command below.

    $ vagrant -v
    Vagrant 2.2.15

## Oracle VM VirtualBox & Vagrant ##

##### Working with Virtual Machines #####

Before starting the entire virtual machine creation process and using vagrant, the user should create a directory where he wants to create the virtual machines. For example, 

    [...]\DevOps> mkdir vagrant-project-1

After creating the directory, the user needs to add a Vagranfile. A typical example of this file is available on the link [atb / vagrant-multi-spring-tut-demo — Bitbucket]. 

Now with the Vagrantfile present on the directory, the user needs to create the virtual machine, through the command below.

    [...]\DevOps\vagrant-project-1> vagrant up  

If the user checks the directory created previously, it is possible to see that in addition to the Vagrantfile, there is now a vagrant directory, which means that the virtual machine was created. Another way to check the virtual machine is through the command below, which gives the user the status of the virtual machine.

    [...]\DevOps\vagrant-project-1> vagrant status
    Current machine states:

    default                   running (virtualbox)

    The VM is running. To stop this VM, you can run `vagrant halt` to
    shut it down forcefully, or you can run `vagrant suspend` to simply
    suspend the virtual machine. In either case, to restart it again,
    simply run `vagrant up`.

If the user accesses the VirtualBox interface it can see the virtual machine created by the vagrant procedure, as described in the image below.

![virtualbox](vb_1.PNG)

Unlike the first part of the report, when using vagrant to create virtual machines, the entire protocol is already configured (such as ssh). Therefore, it is possible to access the virtual machine remotely through the following command.

    vagrant ssh

![virtualbox](vb_2.PNG)  

If the user checks the files that the virtual machine contains, it is possible to see that it only contains the files regarding the installation process, as described in the image above. Therefore, it is necessary to prepare the web virtual machine. To do that, the user needs to stop the virtual machine, through the following command.

    [...]\DevOps\vagrant-project-1> vagrant halt
    ==> default: Attempting graceful shutdown of VM...

    [...]\DevOps\vagrant-project-1> vagrant status
    Current machine states:

    default                   poweroff (virtualbox)

    The VM is powered off. To restart the VM, simply run `vagrant up`

Now it is necessary to change a few characteristics of the Vagrantfile. The user needs to uncomment the following lines.
The uncomment of these will allow the apache install process.

    ...
    config.vm.network "forwarded_port", guest: 80, host: 8080
    ...
    config.vm.network "private_network", ip: "192.168.33.10"
    ...
    config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    apt-get install -y apache2
    SHELL
    ...

To activate the changes made it is necessary to restart the virtual machine. For that, the user can run the command below.

    [...]\DevOps\vagrant-project-1> vagrant up --provision

If the user accesses the virtual machine again, it will appear new files, namely, the index.html file. Index.html is the file (archive), in many web servers, served by default (default) if the requested URL corresponds to a directory. It is usually the first file (archive) on a website.

![virtualbox](vb_3.PNG)  

The Apache Ubuntu Default Page appears using both urls described below.

    http://localhost:8080
    http://192.168.33.10:8080

![virtualbox](vb_4.PNG)

If the user wants to remove the box, it needs to rn«un the following command.

    vagrant box remove envimation/ubuntu-xenial

##### Run the Projects in the Virtual Machines #####

To set up the project in vagrant, the first step is, as in the previous report explained, to create a directory where the user wants to create the virtual machines. After that, the user adds the Vagrantfile to this directory and adds the project (tut-basic directory).
In the next step, it is necessary to set up some configurations and properties. In the build.gralde file, the user needs to have the correct frontend properties. These changes consist in update the last version of the Gradle frontend plugin and added a webpack run script.

    plugins {
	id 'org.springframework.boot' version '2.4.4'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
	id "org.siouan.frontend" version "1.4.1"
    }

    ...

    frontend {
	nodeVersion = "12.13.1"
	assembleScript = "run webpack"
    }


And, in the package.jason, the user needs to have the following properties.

    "scripts": {
      "watch": "webpack --watch -d",
      "webpack": "webpack"
    }

Next, the user needs to access the buid.gradle file again to support for building war file.

    plugins {
	...
	id 'war'
    }
    ...
    dependencies {
    	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    	implementation 'org.springframework.boot:spring-boot-starter-data-rest'
    	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
    	runtimeOnly 'com.h2database:h2'
    	testImplementation('org.springframework.boot:spring-boot-starter-test') {
            exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
            }
        // To support war file for deploying to tomcat
        providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
    }

 After configuration the build.file regarding the war file properties, it is necessary to add a new class to the src/main/... directory called ServletInitializer, which should contain the following code.

    package com.greglturnquist.payroll;

    import org.springframework.boot.builder.SpringApplicationBuilder;
    import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

    public class ServletInitializer extends SpringBootServletInitializer {

        @Override
       protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
       }

    }

In the next step, the user is adding the support for h2 console. For that, the user needs to access the application.properties file and add the following configuration, and add the weballowothers to h2.

    #spring.data.rest.base-path=/api
    spring.data.rest.base-path=/api
    spring.datasource.url=jdbc:h2:mem:jpadb
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console
    spring.h2.console.settings.web-allow-others=true

Now, the user must add an application context path. For that, it needs to access the app.js file and add the following specifications.

    	componentDidMount() { // <2>
	    	client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
	    		this.setState({employees: response.entity._embedded.employees});
	    	});
    	}

And add the next configuration to the application.properties file, in the first row. 

    server.servlet.context-path=/basic-0.0.1-SNAPSHOT


To fix refernce to css in index.html, the user needs to access the index.html file and correct the following lines.

    ...
    <head lang="en">
         <meta charset="UTF-8"/>
         <title>ReactJS + Spring Data REST</title>
         <link rel="stylesheet" href="main.css" />
    </head>
    ...

 In the next step, the user needs to set the remote h2 database access. For that, the user needs to access the application.properties file and put as described below.

    #spring.data.rest.base-path=/api
    server.servlet.context-path=/demo-0.0.1-SNAPSHOT
    spring.data.rest.base-path=/api
    DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    # In the following settings the h2 file is created in /home/vagrant folder
    spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    spring.datasource.driverClassName=org.h2.Driver
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2-console
    spring.h2.console.settings.web-allow-others=true

Finally, the user only needs to configure the application.properties to make the spring drop de database on every execution.

    ...
    spring.datasource.username=sa
    spring.datasource.password=
    spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
    # So that spring will no drop de database on every execution.
    spring.jpa.hibernate.ddl-auto=update
    spring.h2.console.enabled=true
    ...

To upload the changes of the vagrant file and start the creation of the virtual machines, the user must make sure that its Repository is public. Finally, the user only needs to put the link to the repository that they want to clone on the Vagrantfile. Furthermore, it needs to cofrect the path to the .war file fo the project.

![virtualbox](vb_5.PNG)

Now, the user can build the project, through the following command.

    $ ./gradlew build

To inicialize the machine, the user runs the command below.

    $vagrant up

And now the project is running, without problems.

![virtualbox](vb_6.PNG)

![virtualbox](vb_7.PNG)

![virtualbox](vb_8.PNG)


## Hyper V & Vagrant ##

Hyper-V is Microsoft's hardware virtualization product. Hyper-V runs each virtual machine in its own isolated space, which means the user can run more than one virtual machine on the same hardware at the same time. 
Some of Hyper-V main characteristics are:

    * it can establish or expand a private cloud environment
    * it uses  the user hardware more effectively
    * it improves business continuity
    * it establishes or expands a virtual desktop infrastructure (VDI)
    * it makes development and tests more efficient

Hyper-V is available in Windows Server and Windows, as a server role available for x64 versions of Windows Server. The user needs to check if the local system properties are suitable to use this program.

##### Activate Hyper V #####

To activate Hyper V, the user needs to go through the following procedure: Access to Windows Features -> Select Hyper V -> Restart the computer.

![hyperV](hyperv_1.PNG)

For the first time, it is possible the software won't appear on the Windows search menu. If that happens, the user needs to access Windows Administrative Tools, and access HyperV through there.

The HyperV must be run as an administrator so that the virtual machines can be visualized (both web and db).

![hyperV](hyperv_2.PNG)

On the directory where the user wants to create the virtual machines (web and database), it is necessary to add a Vagrantfile with all the correct configurations. For example, it can be a copy of the Vagrantfile of the first part.
After added the Vagrantfile, it is necessary to change a few properties. First, it is necessary to replace 
    
    'envimation/ubuntu-xenial' 

with  
    
    'hashicorp/bionic64'

in all the places of the file.

After that, it is necessary to add the following line, also in the Vagrantfile. These command will allow the user to run Vagrant all the configurations with synced folders configured. This folder will be created (recursively, if it must) if it does not exist. By default, Vagrant mounts the synced folders with the owner/group set to the SSH user and any parent folders set to root.

     config.vm.synced_folder '.', '/vagrant', disabled: true

After all these settings are configurated, the user needs to run the following command, which will create the two virtual machines (web and database) with HyperV. This command has to be run in the same folder where the Vagrantfile is contained.

    $ vagrant up --provider hyperv     

After the process of creating both virtual machines, it is now possible to see them through the Hyper V interface.

![hyperV](hyperv_3.PNG)    

In the next step, it is necessary to configure the project settings with the vagrant settings, regarding the network configuration. For that, the user needs to access the application.properties and change the spring.datasource.url variable, as described below.

    ...
    spring.datasource.url=jdbc:h2:tcp://192.168.106.150:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
    ...

This change occurs at the IPv4 level. In this case, it is necessary to replace it with the IPv4 of the database virtual machine provided by HyperV. To know IPv4, just access HyperV, select the db virtual machine, and Networking. The IPv4 shown is the one that needs to be added to the variable below. As can be seen in the image below, the IPv4 is 192.168.106.150.

![hyperV](hyperv_3.PNG)  

After all the changes were made and saved, the user needs to run the following command so the vagrant updates the new information.

    vagrant reload --provision hyperv

Now, to see the program's front end, the user needs to put the URL below.

    https://<web IPv4><port><fileName.war>
    https://192.168.108.219:8080/demo-0.0.1-SNAPSHOT

 In this case, the IPv4 is the IPv4 of the web virtual machine. As can be seen in the image below, the IPv4 is 192.168.108.219. This address can be accessed, also, through the Hyper V interface.

![hyperV](hyperv_4.PNG)  

Regarding the database server, it can be accessed by the following URL.

    https://<web IPv4>:<port>/<fileName.war>/h2-console
    https://192.168.108.219:8080/demo-0.0.1-SNAPSHOT/h2-console

![hyperV](hyperv_5.PNG)  

As it can be seen in the image above, it is important that the user puts the JBDC URL correct form, as described below.

    jdbc:h2:tcp://<db IPv4>:<port>/./jpadb
    jdbc:h2:tcp://192.168.106.150:9092/./jpadb

Finally, after connecting it will be possible to access the database information.

![hyperV](hyperv_6.PNG)  

## Conclusions ##
Vagrant is a useful tool that allows a user to manage, create and distribute portable development environments. Through any repository containing a Vagrantfile ( a file that has all configuration details, for setting up an entire development environment), it is possible to run a brand new development environment that is ready without any manual intervention.

Vagrant provides resources for a user to create development environments and share the environment with anybody else with ease. It gives, easy management, not only to create them but also to change and delete them.

Regarding the Hyperv, this tool provides an isolated space of its own to run a virtual machine or multiple. Its main characteristics and features are:

    * Computing Environment
    * Backup and Recovery
    * Optimization
    * Portability
    * Remote Connectivity
    * Security


## References ##
    * https://opensource.com/resources/vagrant
    * https://www.vagrantup.com/intro
    * https://www.vmware.com/topics/glossary/content/hypervisor
    * https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v
    * https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/reference/hyper-v-requirements
    * https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/hyper-v-technology-overview
    * https://preemo.com/advantages-and-features-of-microsoft-hyper-v
