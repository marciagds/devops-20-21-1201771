# Class Assignment 3 Report ( Part 1 ) #
___

    Márcia Guedes [1201771@isep.ipp.pt] | may 2021
___

## Virtual Machines ##
A virtual machine (VM) is simply another physical computer or server. It has a CPU, memory, disks to store the user files, and can connect to the internet if needed. However, virtual machines are known as virtual computers or software-defined computers within physical servers, existing only as code. ´

The main process in virtual machines is Virtualization, which consists of running a virtual instance of a computer system in a layer abstracted from the actual hardware. Usually, it refers to running multiple operating systems on a computer system simultaneously. This process can appear as if the applications are running on their own dedicated machine, where the operating system, libraries, and other programs are unique to the guest virtualized system and unconnected to the host operating system which sits below it.

In general, Virtualization allows a computer to emulate other computers or devices, being supported by a hypervisor. A hypervisor, also known as a virtual machine monitor (VMM), is software that creates and runs virtual machines. It allows one host computer to support multiple guest virtual machines by virtually sharing its resources, such as memory and processing.

There are different types of hypervisor, depending on the functionality that it will exercise. For example, there are hypervisors to deal with hardware-level virtualization (interacts with host hardware) or hypervisor to deal with hosted virtualization (interacts with host operatin system).
The main advantages of this process are:

    * Low costs
    * Agility and speed
    * Security benefits
    * Scalability
    * Lowered downtime

## Virtual Box as a Virtual Machines ##

##### Install Oracle VM Virtual Box #####
Oracle VM VirtualBox is cross-platform virtualization software that allows the user to extend their existing computer to run multiple operating systems at the same time.  It is a very good tool for testing, developing, demonstrating, and deploying solutions across multiple platforms on one machine.  Its main advantages are:

    * lightweight 
    * easy to install and use
    * fast and powerful virtualization engine
    * agile
    * excellent performance

Oracle VM VirtualBox is a bridge to open source and cloud development, which is the world’s most popular free and open-source, cross-platform virtualization software.

To understand how to work with Virtual Machines, this example is set using the Oracle VM VirtualBox. To install this software the user must download the program, available at the following link: [https://www.virtualbox.org/wiki/Downloads].

 The following process is described for Windows. After downloading, the user must select the following settings:

    * name: (virtual machine's name)
    * type: Linux
    * version: ubuntu (64-bits)
    * memory size: 2048 MB ()
    * storage on physical hard disk: dynamically allocated

![install_part1](install_part1.PNG)

After the setup, the Virtual Machine (VM) is ready to work.

![vm_part1](vm_part1.PNG)

To initiate a server, it is necessary to connect the server network to the local network. To do that, the user must go to File -> Host Network Manager -> Create. This process will create an IP address for the Virtual Machine. As can be seen in the following image, in this example, the IPv4 address is 192.168.253.1/24.

![vm_part2](vm_part2.PNG)

The next step is configuring the VM settings. The user must go to Settings -> Network and then configure Adapter 1 and Adapter 2, as described in the following image. 

![vm_part3](vm_part3.PNG)

In the following step, the user needs to download an iso file, available at the link: [https://help.ubuntu.com/community/Installation/MinimalCD]. This file works like a light version of ubuntu. After that, it is necessary to add the iso file to the VM. The user needs to go to Settings -> Storage and upload the iso file, as described in the following image. 

![vm_part4](vm_part4.PNG)

Now, the user needs to start the VM and proceed to the ubuntu installation process. During installation, the user must accept the predefined conditions and do next throughout the process. However, extra attention is needed during the final phase of the installation process.

After the message of the successful installation appears, it is necessary to remove the disk (iso file) previously added. Only after the removal, it is possible to finish the process.
After this process, the server is ready to work. During the installation step, it was necessary to create a user and define a password. These are the credentials that the user must enter when trying to authenticate with the VM.

![vm_part5](vm_part5.PNG)

Finally, the user can also set up environment settings, through the following command. 

        $ vi .bashrc

For example, the user can uncomment the following line, which will, in this case, set up colors in the prompt command.

        #force_color_promt=yes

After the changes, the user needs to run the command below.

        $ source .bashrc

##### Set up Virtual Box #####

Before installing any program in the VM, it is necessary to make sure that the VM is updated. "sudo" is what allows the user to run the command as an administrator, and "apt" is a software manager of what software can be installed.

        $ sudo apt update

The next step is to install some software in the virtual machine, in this case, the net-tools package, which includes tools to control and set up the network such as 'arp', 'ifconfig', 'netstat', 'rarp', 'nameif' and 'route'. It is, basically, a network manager of essentials tools (networking toolkit).

        $ sudo apt install net-tools

After installing the network tools, the user must set a few network configurations. For that, the user needs to access the 01-netcfg.yaml, and add the addresses' configuration.

        $ sudo nano /etc/netplan/01-netcfg.yaml

This file should have the structure as shown below. It is very important to put the content in the format since this file is sensitive to the space between the characters. It must follow a specif structure. Regarding the IP address, the user must put an IP address of a possible host. Previously, it was shown that the IPv4 address of the virtual machine was 192.168.253.1/24. So a possible host IPv4 address is 192.168.253.12/24 (for example).

        network:
          version: 2
          renderer: networkd
          ethernets:
            enp0s3:
              dhcp4: yes
            enp0s8:
              addresses:
                - 192.168.253.12/24

To activate these changes, the user needs to run the following command. Through the second command, it is possible for the user to check the network configuration status and if everything is set up correctly.

        $ sudo netplan apply
        $ inconfig

As it can be seen in the next image, the network status shows that the previous configuration was done successfully. The enp0s8 network has now the IP address that it was assigned to it. This network is responsible to allow the VM to communicate with the internet. As for the lo network, it represents the virtual local network, also known as the lop back network. It can be used to access network services locally.

![ifconfig](ifconfig.PNG)

In the next step, the user needs to install the OpenSSH server,  which is a terminal server that allows access to the virtual machine remotely (in a remote session).

        $ sudo apt install openssh-server

After installing it, it is necessary to access sshd_config, through the following command.

        $ sudo ano /etc/ssh/sshd_config

And then, uncomment the line, which contains the below text, by removing the '#' at the beginning of the sentence.

        #PasswordAuthentication yes

To activate the changes made, the user must run the following command. After all the changes are updated in the VM, it is now possible for the user to access the VM through a remote connection.

        $ sudo service ssh restart

For instance, it is possible to access the VM, through the local system Prompt Command or Putty(e.g.), as described in the following image.

![remoteaccess](remote_access.PNG)

Finally, the user needs to install the vsftpd, which is a protocol (protocol FTP) that allows the user to transfer files from and into the virtual machine. It works as a remote file server, unlike OpenSSH which works as a remote terminal.

        $ sudo apt install vsftpd

After that, as in the previous install processes, it is necessary to set up the necessary configurations, through the svftpd.conf file.

        $ sudo nano /etc/vsftpd.conf

The user must uncomment the following line, by removing the '#' symbol at the beginning of the sentence. This property will allow the user to write the script remotely.

        #write_enable=YES

To activate the changes, the user needs to run the following command.

        $ sudo servisse vsftpd restart 

##### Install GIT in the Virtual Box #####

The user can install the git in the virtual machine through the below command.

        $ sudo apt install git
        $ git --version
        git version 2.17.1

##### Install Java in the Virtual Box #####        

The user can install the java in the virtual machine through the below command.

        $ sudo apt install openjdk-8-jdk-headless
        $ java -version
        openjdk version "1.8.0_282"
        OpenJDK Runtime Environment (build 1.8.0_282-8u282-b08-0ubuntu1~18.04-b08)
        OpenJDK 64-Bit Server VM (build 25.282-b08, mixed mode)

##### Install Maven in the Virtual Box #####

The user can install the maven in the virtual machine through the below command, so it can run the program through this build tool.

        $ sudo apt install maven
        $ mvn --version
        Apache Maven 3.6.0
        Maven home: /usr/share/maven
        Java version: 1.8.0_282, vendor: Private Build, runtime: /usr/lib/jvm/java-8-openjdk-amd64/jre
        Default locale: en_US, platform encoding: UTF-8
        OS name: "linux", version: "4.15.0-142-generic", arch: "amd64", family: "unix"

##### Install Gradle in the Virtual Box #####

The user can install gradle in the virtual machine through the below command, so it can run the program through this build tool. For Gradle to work it is necessary to add extra software such as curl, sdkman and zip. 

        $ sudo apt install gradle
        $ sudo apt install curl
        $ curl -s "https://get.sdkman.io" | bash
        $ sudo apt install zip

And finally, it is necessary to create a path for the sdkman software.

        $ source "$HOME/.sdkman/bin/sdkman-init.sh"

To check if the gradle is correctly installed, the user only needs to run the following command.

        $ gradle --version

        ------------------------------------------------------------
        Gradle 6.3
        ------------------------------------------------------------

        Build time:   2020-03-24 19:52:07 UTC
        Revision:     bacd40b727b0130eeac8855ae3f9fd9a0b207c60

        Kotlin:       1.3.70
        Groovy:       2.5.10
        Ant:          Apache Ant(TM) version 1.10.7 compiled on September 1 2019
        JVM:          1.8.0_282 (Private Build 25.282-b08)
        OS:           Linux 4.15.0-142-generic amd64

To install a specif version of gradle, the can run the following command.

        $ sdk install gradle <version>
        $ sdk install gradle 7.0                (e.g.)

##### Run the Projects in the Virtual Box ####      

Now that the virtual machine is all set up, it is possible to run all the previous projects. It is only necessary to clone the repository into the virtual machine.

        $ git clone https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git

###### Run CA1 in the VM ######
Give execution permissions to Maven Wapper.

        $ chmod +x mvnw

Run the project, through Maven Wapper.

        $ ./mvnw spring-boot:run

![ca1_springboot](ca1_sb1.PNG)

![ca1_springboot](ca1_sb2.PNG)

When the project is running, it is possible to check the program's front end. It is only necessary to put the following URL.

        http://<host IPv4>:<port>/
        http://192.168.253.12:8080/     (e.g.)

![ca1_springboot](ca1_sb3.PNG)

Run the project, through Maven.

        $ mvn spring-boot:run

![ca1_springboot](ca1_sb4.PNG)

And the same result appears.

![ca1_springboot](ca1_sb3.PNG)

###### Run CA2 - Part 1 in the VM ######
Give execution permissions to Maven Wapper.

        $ chmod +x gradlew

Build the project, through Gradle Wapper. 

        $ ./gradlew build

![ca2_springboot](ca2_sb1.PNG)

Build the project, through Gradle Wapper. 

        $ gradle build

![ca2_springboot](ca2_sb2.PNG)

Change the build.gradle file.

        $ nano build.gradle

In the runClient task, it is necessary to change the argument by replacing the 'localhost' for the IPv4 address, in this case, '192. 168.253.12'. This change only happens in the project of the local system.

![ca2_springboot](ca2_sb3.PNG)

Run the runServer task in the virtual machine.

        $ ./gradlew runServer
        $ gradle runServer

![ca2_springboot](ca2_sb4.PNG)

Run the runClient task in the virtual machine.

        $ ./gradlew runClient
        $ gradle runClient

![ca2_springboot](ca2_sb5.PNG)

![ca2_springboot](ca2_sb6.PNG)

![ca2_springboot](ca2_sb7.PNG)

It is possible to see the clients through the server.

![ca2_springboot](ca2_sb8.PNG)

###### Run CA2 - Part 2 in the VM ######
Give execution permissions to Maven Wapper.

        $ chmod +x gradlew

Build the project, through Gradle Wapper. 

        $ ./gradlew build

![ca2_springboot](ca2_sb9.PNG)        

Build the project, through Gradle. 

        $ gradlew build

![ca2_springboot](ca2_sb10.PNG)         

In this case the build failed. To run this project, it is necessary to use Gradle 6.3. Therefore, it is necessary to istall gradle with that version.

        $ sdk install gradle 6.3

 ![ca2_springboot](ca2_sb11.PNG)

Rebuild the project, through Gradle

        $ gradlew build

![ca2_springboot](ca2_sb12.PNG)    

Run the project, through Gradle or Gradle Wapper

        $ ./gradlew bootRun
        $ gradle bootRun

![ca2_springboot](ca2_sb13.PNG)