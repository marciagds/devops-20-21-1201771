# Class Assignment 1 Report #

___

    Márcia Guedes [1201771@isep.ipp.pt] | april 2021
___

### Version Control ###
Version Control is a system responsible to manage the changes in files or set of files, over time. It is mainly used to control version code, binary files, and digital assets. It groups a lot of different functionalities, including version control software, version control systems, or version control tools.

Also defined as a software configuration management component, it allows several developers (in distinctive areas) to work together on the same project. 

Version Control as a management track change tool presents several advantages during the project development, such as:

    * revert selected files or an entire project to a previous state
    * compare changes over time
    * trace every change made in the project and its responsible author
    * detect problems
    * assign issues

These advantages establish to DevOps teams a visible and clean environment. It helps improve teamwork and increase product delivery speed.

### Distributed and Centralized Version Control Systems ####    
Nowadays, there are two main types of this control tool: Centralized Version Control Systems (CVCS) and Distributed Version Control Systems (DVCS).

Centralized Version Control's main characteristic is the single server, which contains all the version files. This centralized source is the master repository of all code.

To update or maintain the code, the user must communicate with the server and pull the current version code - a copy will be added to its local system. After the changes, the code is returned to the master server along with all the changes through a commit, merging with the last version. So, this control system provides only a central repository, where all the project versions are stored.

Distributed Version Control works similar to Centralized Version Control, however, instead of one single repository (only server), in this case, each developer or client has their server, which contains all the versions/history of the project along with its local branches and changes. 

These settings provide an independent environment where clients/developers can work locally and disconnected from the master server. A clone of the project contains the entire history of the latest version code stored in the master repository.  Such as Centralized Version Control, after the changes, the latest version can also be sent to the central (master) repository to be saved and available to the other servers.

## 1. GIT as a Distributed Version Control System ##
Among several possible Distributed Version Control Systems, GIT is the most commonly used nowadays. Its main advantages include:

    * offline work, which provides flexibility
    * faster work, since there is no need to communicate with remote servers
    * possibility to work on branches, dividing the project into different parts according to different issues
    * less merging issues when connecting the updated version.

To fully understand GII, all commands, and functionalities, this Version Control System provides an online manual book with all the instructions. It can be accessed through the link: [https://git-scm.com]. 

#### GIT Configurations and Basic Commands ####

##### GIT Install for Windows #####
Check if git already exists in the local system, using the prompt command line. 

      $ git

If the command is not recognized, all the installation processes for its described in the following link: [https://git-scm.com].    

##### GIT Help #####
To present a friendly manipulation and control of all the tools provided, GIT gives a comprehensive manual page where it describes all the available commands and options. 

These commands are accessible in any directory, whether the server is on or offline. It is possible not only to search all the commands but also a specific one, and this one works - all of its options. It is a very helpful GIT tool!

- Dispose of all available GIT commands
    
        $ git help
        $ git --help

- Dispose of helpful information for a specif command (e.g. add)

        $ git help <verb>
        $ git <verb> --help
        $ git add -help             (e.g.)

- Dispose of all command options (e.g. add)

        $ git <verb> -h
        $ git add -h                (e.g.)

##### GIT Setup #####

###### Configurations ######
Git provides a set of tools that allow the user to configure several different variables that control GIT functionalities but also customize the user's GIT local environment.

- Check all settings and their path

        $ git config --list --show-origin

- Check user settings
 
        $ git config --list

- Check a specific user setting (e.g. user.email)
 
        $ git config <setting>
        $ git config user.email


- User Identity - change user's name or email

        $ git config --global user.name <userName>
        $ git config --global user.name <email> 
        $ git config --global user.name "Márcia Guedes"             (e.g.)  
        $ git config --global user.email marciaguedes@example.com   (e.g.)

- Configuration help

        $ git help config

###### Text Editor ######
By default, git's text editor is Vim - text editor program for Unix. However, it is possible to change GIT's text editor to different and more friendly user programs. 

  - If the user changes the text editor to another Unix program, it is necessary to make sure that the text editor is installed. For that, it is only necessary to run the program. If GIT doesn't recognize it, it is necessary to install the program. Come examples are: pico, ATOK, vi, nano ... (e.g. nano)
   
        $ nano
        $ git config --global core.editor nano

  - If the text editor is an app or IDE softare, the git command must considerer the path to where the app is installed in the local system. Some examples are Notepad++, Sublime Text, Visual Studio Code, Atom, ... (e.g Notepad++)

        $ git config --global core.editor "'C:/Program Files/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin"

      More information dispose in the link: [https://docs.github.com/pt/github/getting-started-with-github/associating-text-editors-with-git]

###### .gitignore file ######
By default, when a repository is initialized, GIT creates a .gitignore file. This file is responsible for managing which of the files git should or not track changes of and save the file's history. If it is not important to track changes of a given file, then this file should be referenced in the .gitignore file. 

The .gitignore file is a very important asset mostly due to the compilation files. These files store every information related to the builder process of the project, which is a lot of accumulated non-important information. Therefore, complication files (and others) must be mentioned in the .gitignore files so that GIT knows that they should be ignored when providing a status of the working directory and the master repository. This filter file allows the creation of a lighter and cleaner repository.

There are several approaches to define the .gitignore file. For instance, a useful online resource that can create a .gitignore file based on specific requirements is gitignore.io, which is available in the link: [https://www.toptal.com/developers/gitignore]. Some of the input arguments (related to the project) necessary are the IDE software (e.g. IntelliJ), the builder (e.g. Maven), and the Node.

Nevertheless, the .gitignore file provided by this resource may need additional changes.

##### GIT Basic Commands #####

###### Inicialize a Git Repository ######

- Start a new Local Repository 
    - access to the directory and initialize git

            $ cd C:/Users/switch/Desktop/devops/project
            $ git init

    - initiate files changes' control management

            $ git add *.c
            $ git add LICENSE
            $ git commit -m 'Initial project version'


- Start a new Remote Repository 
    - access to the directory and initialize git

            $ cd C:/Users/switch/Desktop/devops/project
            $ git init

    - initiate files changes' control management

            $ git add *.c
            $ git add LICENSE
            $ git commit -m 'Initial project version'

    - associate with remote Repository

            $ git remote add origin https://marciagds@bitbucket.org/marciagds/teste_devops-2.git

    - send project to online Repository

            $ git status
            $ git add .
            $ git commit -m "initial commit"
            $ git push -u origin master

- Start an existing Repository
    - access to the directory and clone repository

            $ cd C:/Users/switch/Desktop/devops/project
            $ git clone https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git    (e.g.)

    - access to the directory and clone repository in a directory with a specif name (e.g. devOpsProject)

            $ cd C:/Users/user/my_project
            $ git clone https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git devOpsProject  (e.g.)

###### Record Project's Changes: Basic Snapshot Commands ######
When working on a local server, each user keeps its changes locally until they commit the information to the master repository. GIT provides a command to check the status of each file on the project. 
There is four main status possible to classify a file:

Status    | Description
--------- | ------
untracked | The file is not yet add to the master repository. They only exist in the user's working directory. Track changes in this file do not record yet. It is the first version of the code, all the changes done to the file are not save in the project's history. It needs to be added to the repository.
unmodified | The file wasn't changed in the user's working directory. It is still intact (unchanged). No command needs to be done.
modified | The file was changed in the user's working directory. The changes need to be committed to the master repository
staged | The file's changes are complete and the new version is finished, it only needs to be merged with the main project on the master repository. The working directory contains a new version of the project that still isn't in the repository.

- Check Project Files Status 
  
            $ git status

    - GIT Status Response : branch is updated with origin/master

            $ git status
            On branch master
            Your branch is up to date with 'origin/master'.

            nothing to commit, working tree clean

            nothing added to commit but untracked files present (use "git add" to track)

    - GIT Status Response : untrack file on the working directory 

            $ On branch master
            Your branch is up to date with 'origin/master'.

            Untracked files:
               (use "git add <file>..." to include in what will be committed)
            src/main/tests/

            nothing added to commit but untracked files present (use "git add" to track)

    - GIT Status Response : modified file on the working directory

            $ On branch master
            Your branch is up to date with 'origin/master'.

            Changes to be committed:
                (use "git restore --staged <file>..." to unstage)
            new file:   src/main/tests/com/greglturnquist/payroll/EmployeeTest.java
 
    - GIT Status Response : staged modified file and modified file on the working directory

            $ On branch master
            Your branch is up to date with 'origin/master'.

            Changes to be committed:
                (use "git restore --staged <file>..." to unstage)
            new file:   src/main/tests/com/greglturnquist/payroll/EmployeeTest.java

            Changes not staged for commit:
                (use "git add <file>..." to update what will be committed)
                (use "git restore <file>..." to discard changes in working directory)
            modified:   src/main/tests/com/greglturnquist/payroll/EmployeeTest.java

    - GIT Status Response : staged modified file on the working directory

             $ On branch master
             Your branch is ahead of 'origin/master' by 1 commit.
                 (use "git push" to publish your local commits)

             nothing to commit, working tree clean

- Start tracking new file. This command sends the workind directory content to the staging area. (e.g. README)
    - add a specif file
  
             $ git add <file>
             $ git add README       (e.g.)

    - add all files at once
  
             $ git add .

- Undo file changes. 
    - Cancel a previous GIT add command. The file present in the staging area returns to the working directory area, which means is, again, no longer being tracked.(e.g. readme)

             $ git restore --staged <file>
             $ git restore --staged readme.md       (e.g.)
    
        In this case, even though the file's track changes are not activated (i.e. file changes are recorded in the project history) the file's changes are in the current branch of the local working directory.

    - Cancel all changes in the file. This command returns the last snapshot of the file saved in the master repository. It cancels all the file changes in the local working directory.

            $ git restore <file>
            $ git restore readme.md       (e.g.)         

        It is a very dangerous command. All the local file changes are gone (disappear) after the command since it replaces the local file with the remote file. Or, if the file is new and non-existent in the remote repository, it deletes it.   

- Check changes from a file in two different trees. 
    - Compare file from staging area with file in local working directory

            $ git diff 

    - Compare file from staging area with file in the last commit

            $ git diff --staged

    - Compare file from from two different commits

            $ git diff <commit1> <commit2>

    - Compare file from from two different branches

            $ git diff <branch1> <branch2>

    - GIT diff possible responses

        - GIT diff response: information added to one file
  
                  diff --git a/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java b/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java 
                  index 3df1dda..74ca004 100644
                  --- a/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java
                  +++ b/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java
                  @@ -27,4 +27,28 @@ class EmployeeTest {
                  //assert
                  assertNotEquals(expected,employee.getJobTitle());
                  }
                  + 
                  +    @Test
                  +    void setIDEquals() {
                  +    //arrange
                  +        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker");
                  +        Long id = new Long("2222222223");
                  +        Long expected = new Long      ("2222222223");
                  +        //act
                  +        employee.setId(id);
                  +        //assert
                  +        assertEquals(expected,employee.getId());
                  +    }
                    \ No newline at end of file

        - GIT diff response: information added to two different files

                  diff --git a/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java b/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java
                  index 5001a33..539b93c 100644
                  --- a/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java
                  +++ b/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java
                  @@ -37,6 +37,7 @@ public class DatabaseLoader implements CommandLineRunner { // <2>
                  public void run(String... strings) throws Exception { // <4>
                         this.repository.save(new Employee("Frodo", "Baggins", "ring bearer", "worker"));
                         this.repository.save(new Employee("Bilbo", "Baggins", "ring bearer", "worker"));
                  +      this.repository.save(new Employee("Alice",          "Baggins", "ring bearer", "worker"));
                         }
                  }
                  // end::code[]

                  diff --git a/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java b/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java
                  index 3df1dda..74ca004 100644
                  --- a/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java
                  +++ b/ca1/tut-basic/src/main/tests/com/greglturnquist/payroll/EmployeeTest.java
                  @@ -27,4 +27,28 @@ class EmployeeTest {
                  //assert
                       assertNotEquals(expected,employee.getJobTitle());
                  }
                  +
                  +    @Test
                  +    void setIDEquals() {
                  +        //arrange
                  +        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker");
                  +        Long id = new Long("2222222223");
                  +        Long expected = new Long("2222222223");
                  +        //act
                  +        employee.setId(id);
                  +        //assert
                  +        assertEquals(expected,employee.getId());
                  +    }
                    \ No newline at end of file

        - GIT diff response: information removed from file

                  diff --git a/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java b/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java
                  index 539b93c..55cf581 100644
                  --- a/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java
                  +++ b/ca1/tut-basic/src/main/java/com/greglturnquist/payroll/DatabaseLoader.java
                  @@ -35,7 +35,6 @@ public class DatabaseLoader implements CommandLineRunner { // <2>

                       @Override
                     public void run(String... strings) throws Exception { // <4>
                   -               this.repository.save(new Employee("Frodo", "Baggins", "ring bearer", "worker"));
                                   this.repository.save(new Employee("Bilbo", "Baggins", "ring bearer", "worker"));
                                   this.repository.save(new Employee("Alice", "Baggins", "ring bearer", "worker"));
                   }

- Save file's changes

    - Commit with text editor
  
            $ git commit

    - Commit with only git command line

            $ git commit -m "message"
            $ git commit -m "add test to EmployeeTest class"          (e.g.)

    - Commit associated to an issue (e.g. close issue)
  
            $ git commit -m "add test to EmployeeTest class. close issue #3"

         When associating a commit with an issue, both of them are linked (related). It is possible to establish different states of a-n issue during the commit. More information is presented in the link: [https://support.atlassian.com/bitbucket-cloud/docs/resolve-issues-automatically-when-users-push-code/]. 

    - Commit untraked file (git add and git commit simultaneously)

            $ git commit -a -m "message"


###### Record Project's Changes: Share and Update Commands ######    
     
Since many developers can be working on the same project at the same time, each user must have an updated working directory. 

Different commands allow the user to interact with the different working areas: the local working directory, the staging area, and the remote repository. Local directories should be updated and users' new information (file changes) must be sent to the remote repository as soon as possible.

- Fetch master repository current files. All the information present in the master repository is sent to the user's local repository. However, this information is not available on the user's working directory, since the information collected is not merged with the current local files (working directory).

        $ git fetch <remote>
        $ git fetch origin           (e.g.)

- Update working directory according to the master repository. For this command, all the information from the master repository is fetched and immediately merged into the current branch that the user is operating on its local working directory.

         $ git pull         

- Send changes to master repository. Usually, it is used to send the local file changes in the staging area to the remote master repository. It can be sent to a specific branch or the master branch.

         $ git push <remote> <branch>
         $ git push origin master         (e.g.)

     To check the remote to where the files changes are being sent there is the following command:

         $ git remote   

###### Work with branches ######
One of GIT and all Distributed Version Control System's major advantages is the possibility of several different developers can work together at the same time. Usually, this environment is established through the creation of branches. Each user has his branch or branches, where they work in the issues assigned to them. 

This characteristic allows working in several different non-related fronts, without interfering with the other user's issues and code development.

- List current branches on the project. When creating a branch, it is very important to make sure that the name of this new branch is not being used for any other branches in the project.
    - Get list of branches' names in local repository 

            $ git branch --list

    - Get list of branches in local repository with a some information (reference to the last commit made in the branch)

            $ git branch -v

    - Get list of all branches' names in local and remote repository.

            $ git branch -a    

    - Get list of all branches' names in local and remote repository with some information (reference to the last commit made in the branch)

            $ git branch -a -v     

- Create new branch from current working directory.

        $ git branch <name>
        $ git branch email            (e.g.)

     It is very important to know that with this command, the user creates a branch. It does not mean that the user is now working on that branch. To work on a new branch, the user must change his working area to the new branch.

- Change the current working branch. This command makes the user starts to work on the branch.
    - Change to a existen branch

            $ git checkout <name>
            $ git checkout emails       (e.g.)
   
        The same command is also used to return to the master branch.

            $ git checkout master

    - Change to a non-existent branch
  
            $ git checkout -b <name>
            $ git checkout -b mailConflits   (e.g.)   

        In this case, the command creates a new branch and, simultaneously, sends the user to that working branch directory (unlike the basic create branch command).

- Rename the current branch. 
    - Change the name of the current working branch.

            $ git branch -m <name>
            $ git branch -m email          (e.g.)

    - Change the name of a branch.

            $ git branch -m <oldName> <newName> 
            $ git branch -m email phone                (e.g.)

- Merge information from one or more branches, possibly in different repositories. 

  The most important aspect to consider when merging information from two different branches (locals or remotes) is that the merging will occur on the current working branch. In other words, the merge will occur to where the head is pointing to.

        $ git merge <branch>

  For example, if the user wants to merge the changes in the branch where he was working (email) to the master branch the should make sure to checkout to the master branch and then type the following command.
    
        $ git merge email           (e.g.)

- Create/save local branch in the remote repository.

        $ git push --set-upstream <remote> <branch>
        $ git push --set-upstream origin email        (e.g.)

- Example of typical workflow on branch without merge conflits

        $ git branch --list
        * master

        $ git branch test1

        $ git checkout test1
        Switched to branch 'test1'

        $ git pull origin master

     ... (change files)...        
        
        $ git status
        On branch test1
        Untracked files:
        (use "git add <file>..." to include in what will be committed)
              README-md

        nothing added to commit but untracked files present (use "git add" to track)

        $ git add README-md

        $ git commit -m "change readme test file"
        [test1 e7f195c] change readme test file
        1 file changed, 1 insertion(+), 1 deletion(-)

        $ git push
        fatal: The current branch test1 has no upstream branch.
        To push the current branch and set the remote as upstream, use

            git push --set-upstream origin test1

        $ git push --set-upstream origin test1

        Enumerating objects: 9, done.
        Counting objects: 100% (9/9), done.
        Delta compression using up to 8 threads
        Compressing objects: 100% (4/4), done.
        Writing objects: 100% (5/5), 432 bytes | 432.00 KiB/s, done.
        Total 5 (delta 2), reused 0 (delta 0), pack-reused 0
        remote:
        remote: Create pull request for test1:
        remote:   https://bitbucket.org/marciagds/devops-20-21-1201771/pull-requests/new?source=test1&t=1
        remote:
        To https://bitbucket.org/marciagds/devops-20-21-1201771.git
         * [new branch]      test1 -> test1
        Branch 'test1' set up to track remote branch 'test1' from 'origin'.

        $ git checkout master
        Switched to branch 'master'
        Your branch is up to date with 'origin/master'.

        $ git merge test1
        Updating 7fd62da..e7f195c
        Fast-forward
        ca1/tut-basic/README-md | 2 +-
        1 file changed, 1 insertion(+), 1 deletion(-)

        $ git status
        On branch master
        Your branch is ahead of 'origin/master' by 1 commit.
         (use "git push" to publish your local commits)

        nothing to commit, working tree clean

- Example of typical workflow on branch with merge conflits
        
        $ git branch test2

        $ git checkout test2
        Switched to branch 'test2'
      
        $ git pull origin master

     ... (change files)...

        $ git status
        On branch test2
        Changes not staged for commit:
          (use "git add <file>..." to update what will be committed)
          (use "git restore <file>..." to discard changes in working directory)
                modified:   README-md

        no changes added to commit (use "git add" and/or "git commit -a")

        $ git add .
        $ git commit -m "update readme-md file"
        [test2 37addee] update readme-md file
        1 file changed, 1 insertion(+), 1 deletion(-)

        $ git push
        fatal: The current branch test2 has no upstream branch.
        To push the current branch and set the remote as upstream, use

        $ git push --set-upstream origin test2
        $ git push --set-upstream origin test2 
        Enumerating objects: 9, done.
        Counting objects: 100% (9/9), done.
        Delta compression using up to 8 threads
        Compressing objects: 100% (5/5), done.
        Writing objects: 100% (5/5), 510 bytes | 510.00 KiB/s, done.
        Total 5 (delta 2), reused 0 (delta 0), pack-reused 0
        remote:
        remote: Create pull request for test2:
        remote:   https://bitbucket.org/marciagds/devops-20-21-1201771/pull-requests/new?source=test2&t=1
        remote:
        To https://bitbucket.org/marciagds/devops-20-21-1201771.git
              * [new branch]      test2 -> test2
        Branch 'test2' set up to track remote branch 'test2' from 'origin'.

        $ git checkout master
        Switched to branch 'master'
        Your branch is up to date with 'origin/master'.

        $ git merge test2
        CONFLICT (add/add): Merge conflict in ca1/tut-basic/testconflit
        Auto-merging ca1/tut-basic/testconflit
        Automatic merge failed; fix conflicts and then commit the result.

        $ nano testconflit
 
     ... Open text editor ...
     
     - unfixed testconflit document
  
             <<<<<<< HEAD
             Hello World!!!!
             =======
             hello!
             i am trying to create a git conflit!
             >>>>>>> test2

     - fixed testconflit document
  
             Hello World!!!!
             hello!
             i am trying to create a git conflit!

     ... Close text editor ...

        $ git status
        On branch master
        Your branch is up to date with 'origin/master'.

        You have unmerged paths.
          (fix conflicts and run "git commit")
          (use "git merge --abort" to abort the merge)

        Unmerged paths:
          (use "git add <file>..." to mark resolution)
                both added:      testconflit

        no changes added to commit (use "git add" and/or "git commit -a")

        $ git add .

        $ git commit -m "merge branch test2"
        [master e6837e5] merge branch test2

        $ git push
        Enumerating objects: 13, done.
        Counting objects: 100% (13/13), done.
        Delta compression using up to 8 threads
        Compressing objects: 100% (5/5), done.
        Writing objects: 100% (5/5), 503 bytes | 503.00 KiB/s, done.
        Total 5 (delta 2), reused 0 (delta 0), pack-reused 0
        To https://bitbucket.org/marciagds/devops-20-21-1201771.git
           3ec5f16..e6837e5  master -> master

###### Other relevant commands ######
- GIT tag commands
    - Tag in the last commit made (after commit)
        
            $ git tag -a <verison> -m "version description"
            $ git tag -a v.1.1.0 -m "version 1"                 (e.g.)

    - Tag a specif commit made 
        
            $ git tag -a <verison> -m "version description" <commitReference>
            $ git tag -a v.1.1.0 -m "version 1" e6837e576926c50e5d02d9c3f895debcb8342242                                   (e.g.)        

    - Push tag to the remote repository 
        
            $ git push <remote> <version>
            $ git push origin v.1.0.0               (e.g.)

    - List of all the tags in the project
        
            $ git tag

- GIT show commands
    - Check information related to remote Repository

            $ git remote show <remote>
            $ git remote show origin          (e.g.)

    - Check information related to a specif tag

            $ git show <version>
            $ git show v1.0.0                 (e.g.)

- GIT checkout command to access in a previous project version  
    - Check/ See the version
      
            $ git checkout <version>
            $ git checkout <v1.0.0>           (e.g.)

    - Create a branch from a previous version
        
            $ git checkout -b <branchName> <version>
            $ git checkout <email> <v1.0.0>           (e.g.)

- GIT log command. It allows to check the project's reachable recorded history. For example, information related to the project's branches.     
        
        $ git log --oneline --decorate --graph --all  

     Another example is to check all the commits made in the project through the following command

        $ git log

## SVN as a Centralized Version Control System (Alternative)  ##

Centralized Version Control Systems is mainly characterized by easy access control since everything is controlled from one place (the server). 

In a centralized model, the user works on the code while connected to the server itself, which leads to a single source of truth.

#### SVN Configurations and Basic Commands ####

##### SVN Install for Windows #####
- Install SVN on client.
  Check if svn already exists in the local system, using the prompt command line. 

        $ svn --help

     SVN command line is not compatible with Windows PowerShell. So, to run SVN, it is necessary to add a program to the local operating system, which will make the svn commands recognizable to the local prompt command line. A possible program is SLIKSVN, which is available in the link: [https://sliksvn.com/download/].

- Install SVN on server
Check if svn already exists in the server system, using the following command.
       
        $ svn --version

     If not, it is necessary to install. In this case, for Ubuntu servers is:

        $  sudo apt-get install subversion


##### SVN Help #####
SVN provides a comprehensive manual page that allows the user to check and visualize all possible and available commands. 

These commands are accessible in any directory, whether the server is on or offline. It is possible not only to search all the commands but also a specific one, and this one works - all of its options. 
It is a very helpful tool!

- Dispose of all available SVN commands
    
        $ svn help

- Dispose of a specif command and its information (e.g. commit)

        $ svn <operation> --help
        $ svn commit --help             (e.g.)

##### SVN Setup #####

###### Initial Project setup ######
  The start of any project must be done through a local server. The user must set a project layout with a design such as:

      project
      |- branches
      |- tags
      |- trunk
            |- tut-basic
                  |- .idea
                  |- .mvn
                  |- .images
                  |- target
                  |- .src
                  |- ...

   For that, in the user local system, they should create directories to establish this design.

            [...]\DevOps\svn_ca1>mkdir project
            [...]\Desktop\SWITCH\DevOps\svn_ca1>cd project
            [...]\Desktop\SWITCH\DevOps\svn_ca1\project>mkdir tags
            [...]\Desktop\SWITCH\DevOps\svn_ca1\project>mkdir branches
            [...]\Desktop\SWITCH\DevOps\svn_ca1\project>mkdir trunk
            [...]\Desktop\SWITCH\DevOps\svn_ca1\project>dir
            Volume in drive C has no label.
            Volume Serial Number is F898-EFB9

            Directory of [...]\Desktop\SWITCH\DevOps\svn_ca1\project

            06/04/2021  18:09    <DIR>          branches
            06/04/2021  18:09    <DIR>          tags
            06/04/2021  18:09    <DIR>          trunk
               0 File(s)              0 bytes
               5 Dir(s)  886,442,500,096 bytes free

 ... create the project ...

            [...]\Desktop\SWITCH\DevOps\svn_ca1\project>cd trunk

            [...]\Desktop\SWITCH\DevOps\svn_ca1\project\trunk>dir
            Volume in drive C has no label.
            Volume Serial Number is F898-EFB9

            Directory of [...]\Desktop\SWITCH\DevOps\svn_ca1\project\trunk

            06/04/2021  18:15    <DIR>          tut-basic
               0 File(s)              0 bytes
               3 Dir(s)  886,342,201,344 bytes free

###### Server setup ######

  - Create a directory for the repository.

        root@vs261:/# mkdir devops
        root@vs261:/# cd devops
        root@vs261:/devops# mkdir svn
        root@vs261:/devops# cd svn
        root@vs261:/devops/svn# mkdir repository
        root@vs261:/devops/svn# cd repository

  - Create a repository.

        root@vs261:/# svnadmin create /devops/svn/repository

    This command initializes the directory as a remote repository. If the user access this directory, which was previously empty, they will see now 6 files.

          root@vs261:/# cd devops/svn/repository
          root@vs261:/devops/svn/repository# ls -l
          root@vs261:/devops/svn/repository# ls -l
          total 24
          drwxr-xr-x 2 root root 4096 Apr  6 15:14 conf
          drwxr-sr-x 6 root root 4096 Apr  6 15:14 db
          -r--r--r-- 1 root root    2 Apr  6 15:14 format
          drwxr-xr-x 2 root root 4096 Apr  6 15:14 hooks
          drwxr-xr-x 2 root root 4096 Apr  6 15:14 locks
          -rw-r--r-- 1 root root  246 Apr  6 15:14 README.txt

  - Set up the conf file.

          root@vs261:/devops/svn/repository# cd conf
          root@vs261:/devops/svn/repository/conf# ls
          authz  hooks-env.tmpl  passwd  svnserve.conf
          root@vs261:/devops/svn/repository/conf# nano svnserve.conf

    In this case, the text editor used was nano, but it could be any other. After the user accesses to the svnserve.config they uncomment the following lines.
     
          anon-access = read
          auth-access = write
          ...
          password-db = passwrd

  - Inicialize repository.

          root@vs261:/# svnserve -d -r /devops/svn/repository

    To check if the command is well done, the following shows the svnserve running.

          root@vs261:/# ps auxww | fgrep svnserveps auxww | fgrep svnserve
          root       270  0.0  0.8  19496  2256 ?        Ss   Apr06   0:00 svnserve -d -r devops/svn/repository
          root      1791  0.0  0.2   8904   676 pts/4    S+   11:38   0:00 grep -F --color=auto svnserve

  - Create a user. Add a user name and a password to the svnserve.conf file.
      
          root@vs261:/# cd devops/svn/repository/conf
          root@vs261:/devops/svn/repository/conf# nano svnserve.config

      ... add user name and password to the file ...

          ...
          [users]
          username = password
          marcia = cry2getter       (e.g.)

      ... save file.    

  - Import initial project from a local repository.
    In this step, the initial project is added to the remote repository for the first time through a local server. After that, the main project is the one stored in the remote Repository.

          svn import ./project svn://vs261.dei.isep.ipp.pt/tut-basic -m "Initial commit. Project start."

    During this proccess, it is necessary to put the username and password establish on the precious step.
    The user, after this process, can delete its local project, since it won't affect the remote repository's project.

##### SVN Basic Commands for the Client #####

###### Start wotkin on the project ######
- Initialize the project on the client's local system. It will create a copy of the master server project to the user's computer.

          [...]\Desktop\SWITCH\DevOps\project> svn checkout svn://vs261.dei@isep.ipp.pt/repository

###### Record Project's Changes ######
- Check the client project status.

          [...]\Desktop\SWITCH\DevOps\project> svn status

     This command allows the user to see the status of the local project files. Each file presents a symbol or a letter that describes its current situation. For more information, check the following link: [http://svnbook.red-bean.com/en/1.8/svn.ref.svn.c.status.html].

- Add a new file to the remote repository. This command is only used when a file is created in the local system by the user and still doesn´t exist in the server. 

          svn add <fileName>

          (e.g.)
          [...]\Desktop\SWITCH\DevOps\project> dir
          06/04/2021  23:05    <DIR>          repository
                      0 File(s)              0 bytes
                      3 Dir(s)  139,365,466,112 bytes free
          [...]\Desktop\SWITCH\DevOps\project>cd repository/trunk
          [...]\Desktop\SWITCH\DevOps\projec\repository\trunk> notepad test.txt
          [...]\Desktop\SWITCH\DevOps\projec\repository\trunk> svn status
          ?            test.txt
          [...]\Desktop\SWITCH\DevOps\projec\repository\trunk>svn add test.txt
          [...]\Desktop\SWITCH\DevOps\projec\repository\trunk> svn status
          A            test.txt

     In this example, the user adds a new file to the master branch, call test.txt.

- Check difference between to files. For more information, clink on the link: [http://svn.gnu.org.ua/svnbook/svn.ref.svn.c.diff.html].
  
          svn diff <fileName>

- Commit the client's local changes to the server repository. SVN works slightly differently from GIT, because, for this Version Control System it is not necessary to add the changed file before the commit command. If the file already exists in the remote repository, it is only necessary to commit the information. 

          svn commit <fileName> -m "commit message"
          svn commit test.txt -m "add message to file"            (e.g.) 

     Furthermore, the commit command in SVN also works as the git push command. Since there is no staging area for the SVN system, the commit made by the client regarding the file changes is sent directly to the master remote repository (server).

- Update client's project. Unlike GIT. In this case, there aren't stow separate commands to update remote server project changes (git fetch and git pull). In SVN, to update the local project, the command pulls the information from the server and, automatically, merges it into the local project.

          svn update 

###### Work with branches ######

- Start a new branch. In SVN, create a branch correspond to duplicate the project code from the trunk remote server directory to the branch remote server directory. Following this command it is necessary to run the update command, since the copy was made on the remote server - it is necessary to clone that copy to the client local system.

          svn copy <remoteOriginBranch> <remoteNewBranch> -m "commit message"
          svn update

          (e.g.)
          [...]\Desktop\SWITCH\DevOps\project> svn copy svn://vs261.dei.isep.ipp.pt/repository/trunk svn://vs261.dei.isep.ipp.pt/repository/branches/email -m "create email branch" 
          svn update
      
- Merge two different branches. After all the file changes are done, it is necessary to commit the changes and, posteriorly, merge the branch project with the trunk project on the server.

          svn merge <remoteOrigin> <remoteBranch>
          svn merge svn://vs261.dei.isep.ipp.pt/repository/trunk svn://vs261.dei.isep.ipp.pt/repository/branches/email        (e.g.)

     If the merge process provokes any conflict during this process it is necessary to resolve it by hand and run the command to inform that the conflict is resolved.

          svn resolved <fileName>
          svn resolved test.txt           (e.g.)

  - Example of branch merging without conflit
  
          (e.g.)
          [...]\Desktop\SWITCH\DevOps\project\branches> svn status
          M       test.txt

          [...]\Desktop\SWITCH\DevOps\project\branches>svn commit . -m "add information"
          Sending        revision\tut-basic\test.txt
          Transmitting file data ...done
          Committing transaction...
          Committed revision 3.

           [...]\Desktop\SWITCH\DevOps\project\trunk> svn merge svn://vs261dei.isep.ipp.pt/repository/trunk svn://vs261.dei.isep.ipp.pt/repository/branches/revision
           --- Merging differences between repository URLs into '.':
           U    tut-basic\test.txt
           --- Recording mergeinfo for merge between repository URLs into '.':
            G   .  

           [...]\Desktop\SWITCH\DevOps\project\trunk> svn update

  - Example of branch merging with conflit
  
          (e.g.)
          [...]\Desktop\SWITCH\DevOps\project\branches> svn status
          M       email\tut-basic\src\main\java\com\greglturnquist\payroll\DatabaseLoader.java
          M       email\tut-basic\src\main\java\com\greglturnquist\payroll\Employee.java
          M       email\tut-basic\src\main\js\app.js

          [...]\Desktop\SWITCH\DevOps\project\branches>svn commit . -m "add email support"
          Sending        email\tut-basic\src\main\java\com\greglturnquist\payroll\DatabaseLoader.java
          Sending        email\tut-basic\src\main\java\com\greglturnquist\payroll\Employee.java
          Sending        email\tut-basic\src\main\js\app.js
          Transmitting file data ...done
          Committing transaction...
          Committed revision 4.

          [...]\Desktop\SWITCH\DevOps\project\branches>svn update
          Updating '.':
          At revision 4.

          [...]\Desktop\SWITCH\DevOps\project\trunk> svn merge svn://vs261.dei.isep.ipp.pt/repository/trunk svn://vs261.dei.isep.ipp.pt/repository/branches/email
                   --- Merging differences between repository URLs into '.':
          C    tut-basic\.idea\workspace.xml
          --- Recording mergeinfo for merge between repository URLs into '.':
           G   .
          Summary of conflicts:
            Text conflicts: 1
          Merge conflict discovered in file 'tut-basic\.idea\workspace.xml'.
          Select: (p) Postpone, (df) Show diff, (e) Edit file, (m) Merge,
                 (s) Show all options: m
          Merging 'tut-basic\.idea\workspace.xml'.
          Conflicting section found during merge:
          (1) their version (at line 59)                                      |(2) your version (at line 62)
          --------------------------------------------------------------------+-----------------------------------------------------------------
                <workItem from="1617750644200" duration="1220000" />          |      <workItem from="1617750644200" duration="1220000" />
                                                                              |      <workItem from="1617752154198" duration="13000" />
                                                                              |      <workItem from="1617752206088" duration="25000" />
                                                                              |      <workItem from="1617752325242" duration="43000" />
          --------------------------------------------------------------------+-----------------------------------------------------------------
          Select: (1) use their version, (2) use your version,
                  (12) their version first, then yours,
                  (21) your version first, then theirs,
                  (e1) edit their version and use the result,
                  (e2) edit your version and use the result,
                  (eb) edit both versions and use the result,
                  (p) postpone this conflicting section leaving conflict markers,
                 (a) abort file merge and return to main menu: 2
          Merge of 'tut-basic\.idea\workspace.xml' completed.
          Select: (p) Postpone, (df) Show diff, (e) Edit file, (m) Merge,
                  (r) Mark as resolved, (s) Show all options: e
          None of the environment variables SVN_EDITOR, VISUAL or EDITOR are set, and no 'editor-cmd' run-time configuration option was found
          Select: (p) Postpone, (df) Show diff, (e) Edit file, (m) Merge,
                  (r) Mark as resolved, (s) Show all options:

      After this information, user should press the letter p. This option puts the merge conflict on hold.  

                  (r) Mark as resolved, (s) Show all options: p
          Summary of conflicts:
            Text conflicts: 1 

      Fix file with conflit.

           [...]\Desktop\SWITCH\DevOps\project\trunk> notepad tut-basic\.idea\workspace.xml

      After conflit resolved.

           [...]\Desktop\SWITCH\DevOps\project\trunk> 
           Resolved conflicted state of 'tut-basic\.idea\workspace.xml'
           [...]\Desktop\SWITCH\DevOps\project\trunk> svn commit tut-basic\.idea\workspace.xml -m "resolve conflict"     
           Sending        tut-basic\.idea\workspace.xml
           Transmitting file data .done
           Committing transaction...
           Committed revision 6.       

           [...]\Desktop\SWITCH\DevOps\project\trunk> svn update
           Updating '.':
           At revision 6.  

           [...]\Desktop\SWITCH\DevOps\project\trunk> svn merge svn://vs261dei.isep.ipp.pt/repository/trunk svn://vs261.dei.isep.ipp.pt/repository/branches/email
           --- Merging differences between repository URLs into '.':
           U    tut-basic\.idea\workspace.xml
           --- Recording mergeinfo for merge between repository URLs into '.':
            G   .  

           [...]\Desktop\SWITCH\DevOps\project\trunk> svn update 

###### Other relevant commands ######
 - SVN tag command. Unlike GIT, in SVN the tag means to create a copy of the current trunk' project and save it in the tags directory. More information available in the link: [http://svnbook.red-bean.com/en/1.6/svn.branchmerge.tags.html].

           svn copy <trunkProject> <tagName> -m "message"

           (e.g.)
           [...]\Desktop\SWITCH\DevOps\project\trunk> svn copy svn://vs261.dei.isep.ipp.pt/repository/trunk svn://vs261.dei.isep.ipp.pt/repository/tags/v1.0.0 -m "v1.0.0"
           Committing transaction...
           Committed revision 7.

           [...]\Desktop\SWITCH\DevOps\project\trunk> svn update

- SVN show command. It presents all the commits. More information available in the link: [http://svnbook.red-bean.com/en/1.7/svn.ref.svn.c.log.html].

  - Commit information in the current directory

           svn log 

           (e.g.)
           [...]\Desktop\SWITCH\DevOps\project\trunk> svn log
           ------------------------------------------------------------------------
           r6 | username | 2021-04-07 01:07:04 +0100 (Wed, 07 Apr 2021) | 1 line

           resolve conflict
           ------------------------------------------------------------------------
           r2 | username | 2021-04-06 23:34:17 +0100 (Tue, 06 Apr 2021) | 1 line

           add a text file
           ------------------------------------------------------------------------
           r1 | username | 2021-04-06 19:19:20 +0100 (Tue, 06 Apr 2021) | 1 line

           Initial commit, import project to server
           ------------------------------------------------------------------------

  - Commit information of a specif directory

           svn log <directory>

           (e.g.)
           [...]\Desktop\SWITCH\DevOps\project\trunk> svn log svn://vs261.dei.isep.ipp.pt/repository/branches
           ------------------------------------------------------------------------
           r5 | username | 2021-04-07 00:37:37 +0100 (Wed, 07 Apr 2021) | 1 line

           merge branches
           ------------------------------------------------------------------------
           r4 | username | 2021-04-07 00:16:32 +0100 (Wed, 07 Apr 2021) | 1 line

           add email support
           ------------------------------------------------------------------------
           r3 | username | 2021-04-06 23:59:23 +0100 (Tue, 06 Apr 2021) | 1 line

           create email branch
           ------------------------------------------------------------------------
           r1 | username | 2021-04-06 19:19:20 +0100 (Tue, 06 Apr 2021) | 1 line

           Initial commit, import project to server
           ------------------------------------------------------------------------

## Conclusions  ##

Git and SVN are two types of version control systems that allow a user to manage the workflow and project management in coding.  The main difference is that Git is a distributed version control system that uses multiple repositories (including a centralized repository and server), as well as some local repositories, whereas SVN is a centralized version control system, that only presents one centralized repository or server.

Git provides a set of tools very helpful to the user. Through git, each developer has their copy of code on their local like their branch. The repositories only required one .gitignore file, and the user can work even if not connected to any Network.  Git's main features are 

    * Distributed System.
    * Branching.
    * Compatibility.
    * Non-linear Development.
    * Lightweight.
    * Open source

SVN provides a central repository where it is kept a working copy and all the changes and commits made. Even though it requires the user to be connected to a network, it is easier to work, with a simple and better user interface. Its main features are:
Directories are versioned

    * Copying, deleting, and renaming.
    * Free-form versioned metadata .
    * Atomic commits.
    * Branching and tagging.
    * Merge tracking.
    * File locking.    

## References ##

    * https://www.perforce.com/blog/vcs/git-vs-svn-what-difference
    * https://git-scm.com/
    * https://www.tutorialspoint.com/svn/index.htm
    * https://www.geeksforgeeks.org/version-control-systems/
    * https://www.perforce.com/blog/vcs/what-is-version-control
    * https://www.geeksforgeeks.org/difference-between-git-and-svn/




