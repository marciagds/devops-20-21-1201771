# Class Assignment 5 Report ( Part 1 )#

___

    Márcia Guedes [1201771@isep.ipp.pt] | jun 2021
___

### Continuous Integration (CI) and Continuous Devilery (CD) ###

Continuous Integration (CI) and Continous Delivery (CD) is, nowadays, a very common and reliable practice for DevOps teams to deliver code changes more frequently and consistently. It is based on operating principles and methods that provide an agile methodology, where software development teams focus on meeting business requirements, code quality, and security. Also known as CI/CD pipeline, it provides a development compartmentalized in automated steps.

Continuous integration is a set of practices that encourage development teams to implement small changes and check-in code to version control repositories frequently. It provides methods to validate the code's changes, through several tools and platforms, such as consistent and automated ways to build, package, and test applications. 

Continuous delivery starts where continuous integration ends. It automates the delivery of applications to selected infrastructure environments. It is very common for teams to work with several different environments, and continuous delivery is an automated way to send all the required information to each of them.

Since the main goal is to provide new operation project versions to environments, continuous testing is mandatory. New versions need to be completely operational, without any problem, and of high quality. All the testing processes s usually integrated into CI/CD pipelines, which automated regression, performance, and other tests that are executed.

##  Jenkins as Continuous Integration Tool ##

Jenkins, one of the leading open-source automation server, is a continuous integration tool, that offers an easy setup for continuous integration and continuous delivery processes. It is adapted for multiple code language combinations and source code repositories, through pipeline configurations. It also several automated routine development tasks.  Even though Jenkins is organized according to scripts for individual steps, it provides a faster and more robust way to integrate the user's entire chain of build, test, and deployment tools.

Jenkins can be summarized in three main use-cases:

    * Build Automation & Automated Testing (the core of CI)
    * Deploying to Pre-Production Servers
    * Deployment to Production Servers

Jenkins offers delivered by plugins, to help with Continuous Integration (CI). They span five areas: platforms, UI, administration, source code management, and, most frequently, build management. This library of plugins, which are usually community-created and free-and-open-source, presents a lot of useful capabilities.

In short, Jenkins has contributed to an improvement in the functioning of the entire CI/CD process. Before Jenkins:

    * The entire source code was built and then tested. Locating and fixing bugs in the event of build and test failure was difficult and time-consuming, which in turn slows the software delivery process.
    * Developers had to wait for test results.
    *The whole process was manual.

After Jenkins:

    * Every commit made in the source code is built and tested. So, instead of checking the entire source code developers only need to focus on a particular commit, which leads to frequent new software releases.
    * Developers know the test result of every commit made in the source code on the run.
    * The user only needs to commit changes to the source code, and Jenkins will automate the rest of the process.

#### Set up ####

To start, the user should download the jenkins.war file from the link: [https://get.jenkins.io/war-stable/2.277.4/jenkins.war], which will allow the user to run the Jenkins locally on your machine. After that, in the folder where to save this file, the user must run the command below. This terminal must remain intact as long as the user wants Jenkins to be running.

    $ java -j jenkins.war

![image1](img1.PNG)

After running the Jenkins executable, the user must log in. For that, it is necessary to access the page through the link:[http://localhost:8080/login?from=%2F] and put the pass that is available in the previous image, as shown below.

![image2](img2.PNG)

After that, the user just has to install the suggested plugins and create a user.

![image3](img3.PNG)

Após isso, o user pode configurar o jenkins para executar por default no [http://localhost:8080/]

After the log-in process, the user should reload. For that, it is now recommended that the user finishes the operation that was previously running on the command line and reruns the command to run the Jenkins executable. The previous link should now display jenkins' interface, as described in the image below.

![image4](img4.PNG)

#### Iniciate a pipeline ####

To start a job in Jenkins, the user must go to Dashboard and start a 'New Item'. After that, select the 'Pipeline' type and assign it a name, which must be unique.

![image5](img5.PNG)

After that, the user should go to the Pipeline section, and add the script that is described below.

    pipeline {
        agent any

        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'devops_1201771_credentials', url:' https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git'
                }
            }
            
            stage('Assemble') {
                steps {
                dir('ca2/part1/tut-gradle-p1/gradle_basic_demo/') {
                        echo 'Assembling...'
                        bat './gradlew clean build'
                    }
                }      
            }
            
            stage('Test') {
                steps {
                dir('ca2/part1/tut-gradle-p1/gradle_basic_demo/') {
                        echo 'Testing...'
                        bat './gradlew test'
                        junit '*/test-results/test/TEST-basic_demo.AppTest.xml'
                    }
                }     
            }
            
            stage('Archiving') {
                steps {
                dir('ca2/part1/tut-gradle-p1/gradle_basic_demo/') {
                        echo 'Archiving...'
                        archiveArtifacts 'build/libs/*'
                    }
                }     
            }
        }
    }

After that, the user must save the pipeline and build it from the 'Build Now' icon. After the build is complete, all steps should be successfully described in green, as shown in the following image.

![image6](img6.PNG)

However, there are other approaches to creating and running Jenkins jobs. A different approach is, instead of creating the pipeline in Jenkins, the user can import from the repository. In this case, when creating this job, also this one of Pipeline type, the user should select the 'Pipeline script from SCM' option in the 'Pipeline' section, as shown in the following image. After that, you should put the repository and the proper settings, where the pipeline script should be fetched.

In this configuration, the user must pay attention to some details. To start, the user should put the URL to your git repository.
Then you must enter your credentials to access the repository (this process is shown below) and finally, you must enter the correct path to access the Jenkinsfile where the script described above must be contained.

![image7](img7.PNG)
 
After making the build, the result should be the same as in the previous process.

![image10](img10.PNG)

#### Credentials in Jenkins ####

When a given pipeline accesses information from a repository, such as Bitbucket, or the Docker Hub, it is possible to store the credentials in Jenkins, and later incorporate them into the pipeline scripts.

To do this, the user must go to 'Manage Jenkins' -> 'Manage Credentials' -> 'Jenkins' -> 'Global credentials (unrestricted)' -> 'Add Credentials'. After that, the user must put the credentials correctly according to the repository, as described in the image below for the case of Bitbucket.

![image8](img8.PNG)

![image9](img9.PNG)

Thus, these credentials can be called in the pipelines, as demonstrated above. The user should be aware that in the pipeline it must be used the ID assigned during the creation of the credentials.