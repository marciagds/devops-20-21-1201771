# Class Assignment 5 Report ( Part 2 )#

___

    Márcia Guedes [1201771@isep.ipp.pt] | jun 2021
___

##  Jenkins as Continuous Integration Tool (cont) ##

### Jenkins's Pipelines ##

In this second phase, we intend to understand a little better the concepts of how the pipeline can act according to the repository settings. For that, in this first example, it is intended that the user accesses a repository and speaks a job from the project of that repository. Below is the pipeline used.

    pipeline {
        agent any

        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git 'https://bitbucket.org/luisnogueira/gradle_basic_demo'
                }
            }
            stage('Build') {
                steps {
                    echo 'Building...'
                    bat './gradlew clean build'
                }
            }
            stage('Archiving') {
                steps {
                    echo 'Archiving...'
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }

![img1](img1.PNG)

Note that the described pipeline is quite simplistic. It is composed only of 3 stages, which consist in making the repository clone, the application's build, and, in the end, some of the information generated during the build is saved, such as, for example, the application's executable jar file.

Thus, during the next phase, a stage related to testing is added to the pipeline, as shown below. Furthermore, a pipeline was placed where the repository is now private, being necessary to add the credentials, as explained in CA5 part1.

    pipeline {
        agent any

        stages {
            stage('Checkout') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'devops_1201771_credentials', url:' https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git'
                }
            }
            
            stage('Assemble') {
                steps {
                dir('ca2/part1/tut-gradle-p1/gradle_basic_demo/') {
                        echo 'Assembling...'
                        bat './gradlew clean build'
                    }
                }      
            }
            
            stage('Test') {
                steps {
                dir('ca2/part1/tut-gradle-p1/gradle_basic_demo/') {
                        echo 'Testing...'
                        bat './gradlew test'
                        junit '*/test-results/test/TEST-basic_demo.AppTest.xml'
                    }
                }     
            }
            
            stage('Archiving') {
                steps {
                dir('ca2/part1/tut-gradle-p1/gradle_basic_demo/') {
                        echo 'Archiving...'
                        archiveArtifacts 'build/libs/*'
                    }
                }     
            }
        }
    }

![img2](img2.PNG) 

When comparing the two previous images, it is verified that the new pipeline stage allows to store and make available information about the application's tests.

Finally, in the same example, it is possible to run the pipeline from a script from the repository itself, as shown also in CA5 part1.

### Docker & Javadocs in Jenkins' Pipelines ###

In a second phase, the user can repeat the process for a new project, but now in a more complex way. Thus, the following pipeline was defined.

    pipeline {
        agent any

        stages {
            stage('Checkout git') {
                steps {
                    echo 'Checking out...'
                    git credentialsId: 'devops_1201771_credentials', url:' https://marciagds@bitbucket.org/marciagds/devops-20-21-1201771.git'
                }
            }
            
            stage('Assemble') {
                steps {
                dir('ca5/part2/gradle/tut-basic/') {
                        echo 'Assembling...'
                        bat './gradlew clean bootJar'
                bat './gradlew clean bootWar'
                    }
                }      
            }
            
            stage('Test') {
                steps {
                dir('ca5/part2/gradle/tut-basic/') {
                        echo 'Testing...'
                        bat './gradlew test'
                        junit '**/test-results/test/*.xml'
                    }
                }     
            }

        stage('Javadocs') {
                steps {
            dir('ca5/part2/gradle/tut-basic/') {
                        echo 'Javadocs...'
                        bat './gradlew javadoc'
                publishHTML (target: [
                        allowMissing: false,
                        alwaysLinkToLastBuild: false,
                        keepAll: true,
                        reportDir: 'build/docs/javadoc',
                        reportFiles: 'index-all.html',
                        reportName: "HTML Javadoc Report"
                    ])

                    }    
                }
            }      

        stage('Building, Pushing and Deleting (locally) the image') {
                steps {
                    dir ('ca5/part2/') {    		
                        script {
                            echo 'Building the image...'
                            def dockerImage = docker.build("1201771/devops_2021_1201771:${env.BUILD_NUMBER}")
                            echo "build number: ${env.BUILD_NUMBER}"
                            docker.withRegistry('','devops_docker_credentials'){
                                echo 'Pushing the image...'
                                    dockerImage.push()
                            }
                            echo 'Deleting image (locally)'
                            bat "docker rmi 1201771/devops_2021_1201771:${env.BUILD_NUMBER}"
                        }
                    }
                }
        }
            
        stage('Archiving') {
                steps {
                dir('ca5/part2/gradle/tut-basic/') {
                        echo 'Archiving...'
                        archiveArtifacts 'build/libs/*'
                    }
                }     
            }
        }
    }

In this pipeline, new stages have been added. To start with, a new javadoc stage was later added to the testing stage. This stage allows Jenkins to publish all the project's javadocs and publish it to Jenkins. To work, the user must install one of the Jenkins plugins 'HTML Publisher plugin'. When adding this plugin after the build, a new icon should appear where the user can access the generated report.

![img3](img3.PNG)

![img4](img4.PNG)

From the image shown above, it can be seen that the css and hyperlinks of the javadoc report created are not in the proper format. This is due to Jenkins' information and access protection systems. If the user wants to analyze the information with the proper formatting, a solution is for the user to run the jenkins war using the following command.

    $ java -Dhudson.model.DirectoryBrowserSupport.CSP="sandbox allow-same-origin allow-scripts; default-src 'self'; script-src * 'unsafe-eval'; img-src *; style-src * 'unsafe-inline'; font-src * data:" -jar jenkins.war

This way, when performing the new build, the javadoc report will present the correct formatting, as shown below.

![img5](img5.PNG)

Finally, the last part added to the pipeline was related to the Docker. Thus, at an initial stage, the user must create the credentials in Jenkins to access his repository, as explained in CA5 part1.
Thus, the created stage consists of creating an image and then tagging it (with the build number) and pushing it to the repository.

![img6](img6.PNG)

##  Buddy as Continuous Integration Tool (Alternative) ##

Buddy is a CI/CD tool that streamlines automation and delivery with a simple and intuitive UI.
Through Docker containers, fully integrated languages and frameworks and ready-to-use actions for fast deployment, Buddy is a free-to-start commercial tool that can help implement the best practices of DevOps in the easiest way possible.

In Buddy, delivery pipelines are used to build, test and deploy websites and applications. The pipelines consist of actions that can be easily arranged, covering the entire process of web development: fetching dependencies, compiling code, compressing CSS, running Gulp/Grunt, bundling modules with Webpack, among others, to be executed in a specific order.

Regarding configurations, Buddy's build actions use build commands executed in a predefined environment. The commands can be run as SH or BASH scripts. If any command ends with an error, Buddy will stop the execution and mark it as failed.

### Setup ###

To start, the user must register in the application. You can register the application from Bitbucket credentials, for example.

![img7](img7.PNG)

![img8](img8.PNG)

After that, the user can connect to one of your repositories.

![img9](img9.PNG)

![img10](img10.PNG)

### Buddy's Pipeline ###

To start a pipeline, as in Jenkins, the user must create a pipeline whose name must be unique, as shown in the image below. After creating the pipeline, the user can create actions that represent each of the stages.

![img11](img11.PNG)

Below is a typical example of Buddy that intends to simulate the same stages discussed earlier in the report in the Jenkins part.

![img14](img14.PNG)

![img15](img15.PNG)

![img16](img16.PNG)

To start with, the user must create a stage to build the application's executable, as shown below. For that, the user must select the Gradle type action and add the command that was also in the jenkins pipeline. On an important note, when using buddy it is very important to grant execution permissions to gradlew, especially on windows type operating systems.

Then, a command was added to create the war file, as well as the test and javadoc command. Until this stage, the entire pipeline is quite simple and similar to Jenkins. However, as far as javadoc is concerned, unlike Jenkins, it is not possible to publish the report on the Buddy platform. Thus, a way to get around the problem was to create a zip file of the information and send it by email to the user with the information and the report.

In the final phase, an action for the Docker was added. For this track, it is necessary to create a Dockerfile, where the following script must be specified.

    FROM tomcat

    RUN apt-get update -y

    RUN apt-get install -f

    RUN apt-get install git -y

    RUN apt-get install nodejs -y

    RUN apt-get install npm -y

    RUN mkdir -p /tmp/build

    ADD gradle/tut-basic/build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

    EXPOSE 8080

In Buddy, the user must enter the information as shown in the image below.

![img17](img17.PNG)

After the process, the user can verify that his images are present in the Docker Hub.

![img18](img18.PNG)

Finally, the last action taken was an attempt to get around the fact that, unlike Kenkins, Buddy was unable to publish information regarding the project's tests and builds. Thus, the command described below was run and the link with the results was sent to the user's email.

    $ ./gradlew clean test --scan

![img18](img18.PNG)

The image describes how the actions are developed and the information that reaches the user's email.

![img19](img19.PNG)

![img20](img20.PNG)

![img21](img21.PNG)


## Conclusions  ##

In short, Continuous Integration is the practice of merging code into the main branch as frequently as possible. It involves automated testing to ensure the main branch of the code is always stable and error-free. This saves development teams plenty of time when it is time to release the code.

Continuous Delivery is the practice of automating deploy-ready code to a production environment with little to no human intervention. It speeds up the delivery process by automating deployment steps and reducing errors that may frequently show up during the deployment phase.

Jenkins and Buddy are two continuous integration tools that are very user-friendly and easy to use. Jenkins presents more information available, mainly in the resolution of problems and conflicts. Its pipelines are more compact and allow the user more functionalities. It also allows, through its various plugins, to present and display information in a more organized and easily accessible way. It is a very dynamic tool, and with a lot of freedom for the user to work as they want.
However, Buddy is a very easy-to-understand tool. it is very simple and dynamic, with easy implementation of the pipelines. It is more limited in terms of functionality, but allows, in some cases, the user in less direct ways to achieve what is intended.

___ 

## References ##

    * https://www.infoworld.com/article/3271126/what-is-cicd-continuous-integration-and-continuous-delivery-explained.html
    * https://blog.inedo.com/jenkins/everybody-hates-jenkins?utm_source=google&utm_medium=cpc&utm_campaign=JenkinsChaos-Ads&utm_content=Jenkins-Foundations1&utm_term=jenkins%20is&utm_campaign=Jenkins-Ads&utm_source=adwords&utm_medium=ppc&hsa_acc=5810695529&hsa_cam=12879955820&hsa_grp=122657090079&hsa_ad=526855146955&hsa_src=g&hsa_tgt=kwd-310542593682&hsa_kw=jenkins%20is&hsa_mt=b&hsa_net=adwords&hsa_ver=3&gclid=Cj0KCQjw5uWGBhCTARIsAL70sLLFdg3z0Ubjapsr35HPh19UmBRQthrQ_HHemOgPRf_Bvonzb5EQ80QaAmKdEALw_wcB
    * https://www.infoworld.com/article/3239666/what-is-jenkins-the-ci-server-explained.html 
    * https://www.ericsson.com/en/ci-cd?gclid=Cj0KCQjw5uWGBhCTARIsAL70sLKx77oQBvcPv_02fZ3vk9miZ9MQ1OCCwlawhsUBlmaraAFIGxcCp0waAmOJEALw_wcB&gclsrc=aw.ds
    * https://about.gitlab.com/topics/ci-cd/pipeline-as-code/
    * https://www.infoworld.com/article/3239666/what-is-jenkins-the-ci-server-explained.html
    * https://www.edureka.co/blog/what-is-jenkins/
    * https://lo-victoria.com/introduction-to-devops-with-buddy







