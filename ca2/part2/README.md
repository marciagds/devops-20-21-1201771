# Class Assignment 2 Report ( Part 2 )#

___

    Márcia Guedes [1201771@isep.ipp.pt] | april 2021
___

## Gradle as a Build Tool (cont) ####

##### Start Spring Boot with Gradle #####
To initialize a Spring Boot project with Gradle, the user must set the configuration properties of the project. For this, the user must access the link [https://start.spring.io/] and select the appropriate characteristics of the project. In the example used, the project settings are designed for:

    * Language: java
    * Spring Boot: 2.4.5
    * Packaging: jar
    * Dependencies: Rest Repositories, Thymeleaf, Spring Data JPA, and H2 Database.

If the user wants to start a new project all the configurations are done. If the user wants to apply these settings to a previous project, they only need to substitute de new src content file for the previous project src content file. 

Furthermore, it is also necessary to add the webpack.config.js and the package.json at the same level as the src content file, since both these files are not available from the previous link. To verify if all the settings are correctly made, the user should run the command above.

    $ gradle tasks
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca2\part2\tut-basic>gradlew tasks

    Welcome to Gradle 6.8.3!

    Here are the highlights of this release:
    - Faster Kotlin DSL script compilation
    - Vendor selection for Java toolchains
    - Convenient execution of tasks in composite builds
    - Consistent dependency resolution

    For more details see https://docs.gradle.org/6.8.3/release-notes.html

    Starting a Gradle Daemon, 1 stopped Daemon could not be reused, use --status for details

    > Task :tasks

    ------------------------------------------------------------
    Tasks runnable from root project 'demo'
    ------------------------------------------------------------

    Application tasks
    -----------------
    bootRun - Runs this project as a Spring Boot application.

    Build tasks
    -----------
    assemble - Assembles the outputs of this project.
    bootBuildImage - Builds an OCI image of the application using the output of the bootJar task
    bootJar - Assembles an executable jar archive containing the main classes and their dependencies.
    bootJarMainClassName - Resolves the name of the application's main class for the bootJar task.
    bootRunMainClassName - Resolves the name of the application's main class for the bootRun task.
    build - Assembles and tests this project.
    buildDependents - Assembles and tests this project and all projects that depend on it.
    buildNeeded - Assembles and tests this project and all projects it depends on.
    classes - Assembles main classes.
    clean - Deletes the build directory.
    jar - Assembles a jar archive containing the main classes.
    testClasses - Assembles test classes.

    Build Setup tasks
    -----------------
    init - Initializes a new Gradle build.
    wrapper - Generates Gradle wrapper files.

    Documentation tasks
    -------------------
    javadoc - Generates Javadoc API documentation for the main source code.

    Help tasks
    ----------
    buildEnvironment - Displays all buildscript dependencies declared in root project 'demo'.
    dependencies - Displays all dependencies declared in root project 'demo'.
    dependencyInsight - Displays the insight into a specific dependency in root project 'demo'.
    dependencyManagement - Displays the dependency management declared in root project 'demo'.
    help - Displays a help message.
    javaToolchains - Displays the detected java toolchains. [incubating]
    outgoingVariants - Displays the outgoing variants of root project 'demo'.
    projects - Displays the sub-projects of root project 'demo'.
    properties - Displays the properties of root project 'demo'.
    tasks - Displays the tasks runnable from root project 'demo'.

    Verification tasks
    ------------------
    check - Runs all checks.
    test - Runs the unit tests.

    Rules
    -----
    Pattern: clean<TaskName>: Cleans the output files of a task.
    Pattern: build<ConfigurationName>: Assembles the artifacts of a configuration.
    Pattern: upload<ConfigurationName>: Assembles and uploads the artifacts belonging to a configuration.

    To see all tasks and more detail, run gradlew tasks --all

    To see more detail about a task, run gradlew help --task <task>

    BUILD SUCCESSFUL in 6s
    1 actionable task: 1 executed    

If the user is configuring an old project, it is very likely that in the source content ('src/main/resources/static/') exists a directory named build. This directory is generated every time the user builds the project, so it can be deleted. During the next build, it will be created again.
Finally, the user can build the project.

    $ gradlew build
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic>gradlew build

    BUILD SUCCESSFUL in 1s
    6 actionable tasks: 2 executed, 4 up-to-date

##### Run Spring Boot #####
To start the Spring Boot, the local host used by the project must be available. After running the command, the localhost is now showing that the local host is being used by the app - it shouldn't appear an error message when loading the page. However, the webpage is empty!

    $ gradlew bootRun
    (e.g.)
    [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic> gradlew build 
    [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic> gradlew bootRun

##### build.gradle file #####
When comparing a maven project to a Gradle project, the user can see that the Gradle build tool is much more simple.

As it can be seen in the above image, the build.gradle file presents several topics regarding the project configuration:

    * plugins
    * group, version, and source compatibility (which is compatible with the java settings selected during the initial phase).
    * repositories
    * dependencies, which are related also to the previous settings made during the Spring Boot initialization (Rest Repositories, Thymeleaf, Spring Data JPA, and H2 Database)
    * tests dependencies

![build.gradle_file](build.gradle_file.PNG)

Currently in the build.gradle file, there isn't any plugin that allows the project to work with the frontend or react parts, which is why - as previously explained - the webpage is empty. To work with these parts, plugins should be added to this file.

##### Add Plugins #####
To be able to interact/work with javascript files, react and frontend level, it is necessary to add plugins. Therefore, to access and work with these files, the user must:

- Add in the build.gradle file the following plugin. More information is available in the link [[https://github.com/Siouan/frontend-gradle-plugin].
 
		id "org.siouan.frontend" version "1.4.1"

- Add to the same file the node version and the assembleScript configuration.
  
                frontend {
                nodeVersion = "12.13.1"
                assembleScript = "run webpack"
                }

   The user build.gradle file should be similar to what is shown in the above image.

![frontend_plugin](frontend_plugin.PNG)

- Add to the package.json file the webpack configuration.

                "scripts": {
                   "watch": "webpack --watch -d",
                   "webpack": "webpack"
                },

- Finally, the user should build the project again to check if all the settings were correctly added, and then run Spring Boot again.

        [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic>gradlew build

        > Task :installNode
        [installNode] Removing install directory '[...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic\node'.
        [installNode] Downloading distribution at 'https://nodejs.org/dist/v12.13.1/node-v12.13.1-win-x64.zip'
        [installNode] Downloading checksums at 'https://nodejs.org/dist/v12.13.1/SHASUMS256.txt'
        [installNode] Verifying distribution integrity
        [installNode] Moving distribution into '[...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic\node'ild\tmp\installNode\extract'
        [installNode] Removing explode di       rectory '[...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic\build\tmp\installNode\extract'
        [installNode] Removing distribution file '[...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic\build\tmp\installNode\node-v12.13.1-win-x64.zip'
        [installNode] Distribution installed in [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic\node'

        > Task :installFrontend
        [installFrontend] Running 'cmd with args: [/c], ["[...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic\node\npm.cmd" install]
        npm WARN deprecated chokidar@2.1.8: Chokidar 2 will break on node v14+. Upgrade to chokidar 3 with 15x less dependencies.
        npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
        npm WARN deprecated fsevents@1.2.13: fsevents 1 will break on node v14+ and could be using insecure binaries. Upgrade to fsevents 2.
        npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
        npm notice created a lockfile as package-lock.json. You should commit this file.
        npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@~2.3.1 (node_modules\chokidar\node_modules\fsevents):
        npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.3.2: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})
        npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules\watchpack-chokidar2\node_modules\chokidar\node_modules\fsevents):
        npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})

        added 570 packages from 260 contributors and audited 572 packages in 11.054s
        found 0 vulnerabilities


        > Task :assembleFrontend
        [assembleFrontend] Running 'cmd with args: [/c], ["[...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic\node\npm.cmd" run webpack]

        > spring-data-rest-and-reactjs@0.1.0 webpack [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic
        > webpack

        Hash: a2c31be41fec2b65cd58
        Version: webpack 4.46.0
        Time: 2258ms
        Built at: 04/15/2021 14:31:17
                                                Asset      Size  Chunks                   Chunk Names
        ./src/main/resources/static/built/bundle.js  1.08 MiB    main  [emitted]        main
        ./src/main/resources/static/built/bundle.js.map  1.26 MiB    main  [emitted] [dev]  main
        Entrypoint main = ./src/main/resources/static/built/bundle.js ./src/main/resources/static/built/bundle.js.map
        [0] vertx (ignored) 15 bytes {main} [built]
        [./node_modules/webpack/buildin/amd-define.js] (webpack)/buildin/amd-define.js 85 bytes {main} [built]
        [./src/main/js/api/uriListConverter.js] 614 bytes {main} [built]
        [./src/main/js/api/uriTemplateInterceptor.js] 497 bytes {main} [built]
        [./src/main/js/app.js] 6.16 KiB {main} [built]
        [./src/main/js/client.js] 712 bytes {main} [built]
        + 55 hidden modules

        Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
        Use '--warning-mode all' to show the individual deprecation warnings.
        See https://docs.gradle.org/6.8.3/userguide/command_line_interface.html#sec:command_line_warnings

        BUILD SUCCESSFUL in 39s
        7 actionable tasks: 7 executed

        [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic>gradlew bootRun

     This plugin is based on maven, available at the following link: [https://github.com/eirslett/frontend-maven-plugin].  Furthermore, this plugin works with Node, so for the first time, it can be necessary to install it. The localhost is not empty anymore.

##### Add Tasks #####
As explained in the first part of the report, tasks can be added to the project to be implemented when the user wants. Here are some more examples.

###### Copy Generater Jar File ######
Using the same base as in the previous report, a possible task to create a backup directory to save a copy of the generated jar file. Add the new task into the build.gradle file.

        //task to create a backup file of generated jar to dist
        task copyJar(type: Copy) {

                group = "DevOps"

                description = "Copy generated jar file to a backup package name dist"

                from ('build/libs/')
                into ('dist/')
        }

To run the task, the user should run the following command.

        [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic>gradlew copyJar


###### Delete Built Directory ######
A typical delete task is shown above. Unlike the previous example,  in this case, the task is an addiction to the clean task, which is an existent task from Gradle. In this example, the Gradle task clean is going to incorporate a new assign, which is to remove the built file in the src content. As explained before, this content directory is generated during the build process.

        //task to delete built directory
        task deleteBuilt(type:Delete){

                group= "DevOps"
                description = "delete built folder and its contents "
                delete "/src/main/resources/static/built"

        }

        clean.configure {
                dependsOn(deleteBuilt)
        }

In the prompt command line, the user only needs to run the clean task (general), since the deleteBuilt task depends on this one.:

        [...]\DevOps\devops-20-21-1201771\ca2\part2\gradle\tut-basic>gradlew clean
        BUILD SUCCESSFUL in 1sFIGURING [29ms]
        2 actionable tasks: 2 executed
        2 actionable tasks: 2 executed


## Ant as a Build Tool (Alternative) ####

Apache Ant is a java based build tool. It allows developers to adopt agile principles and test-driven development. It is written in XML and one of its main advantages is being open-source,  portable, and easy to understand. 

Some of its characteristics are:
      
    * Allows the user to run builds on both Windows and UNIX/Linux systems.
    * Offers an extensive range of predefined tasks
    * Allows you to invoke from the command line which can easily integrate with free and commercial IDEs.
    * Offers Extensible Architecture
    * Offers Backward Compatibility
 
##### Start Spring Boot with Ant #####

To work with ant software, the user needs to install it in the local system. For more information on this process, the user can access the following link: [https://ant.apache.org/manual/install.html]

##### Start Spring Boot with Ant #####

As exemplified in the previous build tool, to start a project with spring-boot it is necessary to start the project. Again, the user can access the following link to start the spring boot project with specific configurations [https://start.spring.io/]. 

Similar to the first part, the user should select the same properties as before. Spring Boot Application works with Maven or Gradle Projects. For this example, the user can choose either one of them. (In this case, it was selected the Maven Project).

    * Language: java -version 8
    * Spring Boot: 2.4.5
    * Packaging: jar
    * Dependencies: Rest Repositories, Thymeleaf, Spring Data JPA, and H2 Database.

After that, also as before, the webpack.config.js and the package.json should be added to the project.

##### Migrate Project from Maven to Ant #####
As demonstrated before, the Spring boot only works with Maven or Gradle Projects, which presents a small limitation when using ant as a built tool. Therefore, it is not possible to initialize a Spring Boot using Ant Project. If the user wants to work with the ant builder, a possible solution is to migrate the project to the ant, after its creation.

To migrate a project to ant, it is necessary to run the following command. More information at the following link: [https://mkyong.com/maven/how-do-create-an-ant-build-file-from-maven-pomxml/].
        
        $ mvn ant:ant
        [INFO] Scanning for projects...
        [INFO]
        [INFO] --------------------------< com.example:demo >--------------------------
        [INFO] Building demo 0.0.1-SNAPSHOT
        [INFO] --------------------------------[ jar ]---------------------------------
        [INFO]
        [INFO] --- maven-ant-plugin:2.4:ant (default-cli) @ demo ---
        [INFO] Wrote Ant project for demo to [...]\DevOps\devops-20-21-1201771\ca2\part2\ant
        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD SUCCESS
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time:  1.520 s
        [INFO] Finished at: 2021-04-19T22:22:16+01:00
        [INFO] ------------------------------------------------------------------------

This command will generate 3 files: 

    * build.xml
    * maven-build.properties
    * maven-build.xml

If the user wants, both build.xml and maven-build.xml can be merged into one file.

After these 3 files are generated, the project is now working with ant as a build tool, which means that the user can now delete all the maven files or directories present in the project. These files, usually, are:

    * pom.xml
    * mvnw
    * mvnx.cmd
    * ./mvn/

Once the project maven files are deleted, maven commands will not be available anymore, such as the build command.

        $ mvn build
        [INFO] Scanning for projects...
        [INFO] ------------------------------------------------------------------------
        [INFO] BUILD FAILURE
        [INFO] ------------------------------------------------------------------------
        [INFO] Total time:  0.069 s
        [INFO] Finished at: 2021-04-20T20:54:04+01:00
        [INFO] ------------------------------------------------------------------------
        [ERROR] The goal you specified requires a project to execute but there is no POM in this directory ([...]\DevOps\devops-20-21-1201771\ca2\part2\ant). Please verify you invoked Maven from the correct directory. -> [Help 1]
        [ERROR]
        [ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
        [ERROR] Re-run Maven using the -X switch to enable full debug logging.
        [ERROR]
        [ERROR] For more information about the errors and possible solutions, please read the following articles:
        [ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MissingProjectException

During the migration process, some dependencies can be affected. One important one that the user must be careful of is the java version. The user must check in the maven-build.file if it has the correct version.

        ...
        <target name="compile" depends="get-deps" description="Compile the code">
          <mkdir dir="${maven.build.outputDir}"/>
          <javac destdir="${maven.build.outputDir}" 
          ...
          target="1.8"
          ...
          source="1.8">
        <src>
        ...

##### Check Available Ant Targets #####
In ant, the tasks are defined as targets, unlike in gradle.
And, if the user wants to see the predefined targets that ant provides, they only need to run the following command.

        $ ant -projecthelp
        Buildfile: [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\tut-basic\build.xml

        Main targets:

        clean          Clean the output directory
        compile        Compile the code
        compile-tests  Compile the test code
        get-deps       Download all dependencies
        jar            Builds the jar for the application
        javadoc        Generates the Javadoc of the application
        package        Package the application
        test           Run the test cases
        Default target: package


##### Check Available Ant Targets #####
To build the project with ant, the user must run the following command.

        $ ant compile
        Buildfile: [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\tut-basic\build.xml

        test-offline:
        ...
        compile:
        [javac] [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\tut-basic\maven-build.xml:259: warning: 'includeantruntime' was not set, defaulting to build.sysclasspath=last; set to false for repeatable builds
        [copy] Copying 2 files to [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\tut-basic\target\classes

        BUILD SUCCESSFUL
        Total time: 8 seconds

##### Check Available Ant Targets #####

Same as Gradle, the user must create the target in the build.xml file.However, the target's development is more complex since is designed with XML language.Similar to Gradle, to run the targets, the user only needs to run the following command.

        $ ant <targetName>
        $ ant copy              (e.g.)

###### Copy File Target ######
A typical example for a copy target is described above. The user only needs to add the target to the build.xml file. 

    <!-- copy genereted jar file to dist target -->
         <target name="copyJar">
                 <copy todir="dist" file="target/demo-0.0.1-SNAPSHOT.jar"/>
         </target>

And then run the task on the command line. 

        $ ant copyJar
        Buildfile: [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\build.xml

        copyJar:
        [copy] Copying 1 file to [...]\SWITCH\DevOps\devops-20-21-1201771\ca2\part2\ant\dist

        BUILD SUCCESSFUL
        Total time: 0 seconds

###### Delete Directory or File Target ######
A typical example for a delete target is described above. In this case, the user will delete a full directory (package). This specific target will be added to the ant clean target. In other words, when the user runs the clean target, this target will depend on the personalized delete target. It is necessary to add this dependency to the clean target. Also shown above.

        <!--delete the built package target -->
        <target name="deleteBuilt">
        <delete dir="src/main/resources/static/built"/>
        </target>


        <!-- ====================================================================== -->
        <!-- Cleaning up target                                                     -->
        <!-- ====================================================================== -->
        
        <target name="clean" description="Clean the output directory" depends="deleteBuilt">
        <delete dir="${maven.build.dir}"/>
        </target>

And then run the task on the command line. 

        $ ant clean
        Buildfile: [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\build.xml

        deleteBuilt:
        [delete] Deleting directory [...]DevOps\devops-20-21-1201771\ca2\part2\ant\src\main\resources\static\built

        clean:
        [delete] Deleting directory [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\target

        BUILD SUCCESSFUL
        Total time: 0 seconds

If the user only wants to delete a specific file (not a directory), it should create a target as shown above.

        <!--task to delete file -->
        <target name="deleteFile">
        <delete file="Tests.txt"/>
        </target>


##### Initialize Spring Boot in the Command Line #####

The entire process described so far allowed the user to run the spring boot via the IDE. However, it was not possible from the command line. For that, it is necessary to configure buil.xml.
To start, the user must add the following target to the build.xml file.

        <target name="springboot" depends="compile">
                <spring-boot:exejar destfile="target/demo-0.0.1-SNAPSHOT.jar" classes="target/classes">
                <spring-boot:lib>
                        <fileset dir="lib/runtime" />
                </spring-boot:lib>
                </spring-boot:exejar>
        </target>

Furthermore, the user must add the following dependencies to the same file. These specifications allow you to contact the ant library correctly.

        <path id="maven-ant-tasks.classpath" path="lib/maven-ant-tasks-2.1.3.jar" />
        <typedef resource="org/apache/maven/artifact/ant/antlib.xml" uri="antlib:org.apache.maven.artifact.ant" classpathref="maven-ant-tasks.classpath" />

It is also necessary to create an ivy.xml file. This file usually contains the description of the dependencies of a module, its published artifacts and its configurations.    

        <?xml version="1.0"?>
        -<ivy-module version="2.0">
                <info module="spring-boot-sample-ant" organisation="org.springframework.boot"/>
                -<configurations>
                     <conf description="everything needed to compile this module" name="compile"/>
                     <conf description="everything needed to run this module" name="runtime" extends="compile"/>
                 </configurations>
                -<dependencies>
                      <dependency name="spring-boot-starter" conf="compile" rev="${spring-boot.version}" org="org.springframework.boot"/>
                      <dependency name="spring-boot-starter-thymeleaf" conf="compile" rev="${spring-boot.version}" org="org.springframework.boot"/>
                      <dependency name="spring-boot-starter-data-jpa" conf="compile" rev="${spring-boot.version}" org="org.springframework.boot"/>
                      <dependency name="spring-boot-starter-test" conf="compile" rev="${spring-boot.version}" org="org.springframework.boot"/>
                     -<dependency name="junit" rev="4.12" org="junit">
                         <artifact name="junit" type="jar"/>
                      </dependency>
                      <dependency name="spring-boot-starter-web" conf="compile" rev="${spring-boot.version}" org="org.springframework.boot"/>
                      <dependency name="spring-boot-starter-data-rest" conf="compile" rev="${spring-boot.version}" org="org.springframework.boot"/>
                      <dependency name="spring-boot-antlib" rev="2.4.4" org="org.springframework.boot"/>
                      <dependency name="h2" rev="1.4.200" org="com.h2database"/>
                 </dependencies>
         </ivy-module>

And finally, and the information regarding the project.

        <project xmlns:ivy="antlib:org.apache.ivy.ant"
        xmlns:spring-boot="antlib:org.springframework.boot.ant"
        name="react-and-spring-data-rest-basic" default="build" basedir=".">

After that, the user needs to compile the target to check if it was created successfully.

        $ ant sprinboot
        ...
        springboot:
        Trying to override old definition of task antlib:org.springframework.boot.ant:exejar
           [echo] Using start class com.greglturnquist.payroll.ReactAndSpringDataRestApplication
           [echo] Using destination directory [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\tut-basic\target
           [echo] Extracting spring-boot-loader to [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\tut-basic\target/dependency
           [copy] Copying 1 resource to [...]\DevOps\devops-20-21-1201771\ca2\part2\ant\tut-basic\target\dependency
           [echo] Embedding spring-boot-loader v${spring-boot.version}...

        BUILD SUCCESSFUL
        Total time: 8 seconds

And finally, the user only needs to run the target. 

        $ java -jar <path/file.jar>
        $ java -jar target/demo-0.0.1-SNAPSHOT.jar      (e.g.)

![springboot](springboot.PNG)

And now the frontend is working and the localhost is not an empty page anymore.

![webPage](spring-boot.PNG)

Same as Gradle, it is possible to run Spring Boot from the command line.
Furthermore, two different tools allow the user to compile the project differently and run the Spring Boot Application, both on the IDE and the command line.
To conclude, the Spring Boot application can be compiled with MAVEN, GRADLE, or even ANT!

## Conclusions ##
Both, Gradle as Ant, are build automation tools that allow the user to build almost any type of software. However, they present very different characteristics and work functionalities, being Gradle a more easy and efficient tool. 

Gradle's main characteristics and features:
     
    * uses a Groovy-based build automation tool
    * the scripts are built through DSL (Domain Specific Language) 
    * its plugins are coded in Java or Groovy programming language
    * provides a structured build, that is sufficient support for IDE integration
    * high flexibility and standardization
    * supports multi-project build.

Ant's main characteristics and features:

    * uses a Java-based build automation tool
    * the scripts are built through XML (Extensible Markup Language) files
    * doesn't require any coding convention or project structure
    * doesn't support multi-project build


## References ##
    * https://gradle.org/
    * https://ant.apache.org/ 
    * https://docs.gradle.org/current/userguide/what_is_gradle.html
    * https://ant.apache.org/manual/install.html
    * https://mkyong.com/maven/how-do-create-an-ant-build-file-from-maven-pomxml/
    * https://ant.apache.org/manual/install.html
    * https://www.baeldung.com/ant-maven-gradle
    * https://www.javatpoint.com/gradle-vs-ant
    
