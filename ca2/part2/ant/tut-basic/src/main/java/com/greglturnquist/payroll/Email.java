package com.greglturnquist.payroll;

import java.util.regex.Pattern;

public class Email {
    //Attributes
    private String email;

    //Constructor Method
    public Email(String emailAddress) {
        try {
            isValid(emailAddress);
            this.email = emailAddress;
        } catch (Exception e) {
            this.email = "error. Invalid email";
        }
    }
    // Business Methods

    /**
     * Method to validate the email address
     *
     * @param emailAddress that is going to be added
     */
    private static void isValid(String emailAddress) {
        if (emailAddress == null) {
            throw new IllegalArgumentException("The email Address can't be null");
        }
        if (emailAddress.trim().length() == 0) {
            throw new IllegalArgumentException("The email Address can't have blank spaces.");
        }
        if (!checkFormat(emailAddress)) {
            throw new IllegalArgumentException("The email Address is not in the correct format.");
        }
    }

    /**
     * Method to verify if the email has the correct format
     * example: newEmail@gmail.com
     *
     * @param emailAddress - email address
     * @return true if the email is on the correct format
     */
    // Adapted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
    private static boolean checkFormat(String emailAddress) {
        String emailRegex = "[A-Z0-9a-z._%-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";

        Pattern pat = Pattern.compile(emailRegex);
        return pat.matcher(emailAddress).matches();
    }

    public String getEmail(){
        return this.email;
    }
}
