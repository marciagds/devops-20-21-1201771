package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {

    @Test
    public void setJobTitleEquals() {
        //arrange
        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker",22, "234");
        String expected = "boss";
        //act
        employee.setJobTitle("boss");
        //assert
        assertEquals(expected,employee.getJobTitle());
    }

    @Test
    public void setJobTitleNotEquals() {
        //arrange
        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker",22, "bilbo@gamil.com");
        String expected = "boss";
        //act
        employee.setJobTitle("Boss");
        //assert
        assertNotEquals(expected,employee.getJobTitle());
    }

    @Test
    public void setIDEquals() {
        //arrange
        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker",89, "bilbo@gamil.com");
        Long id = new Long("2222222223");
        Long expected = new Long("2222222223");
        //act
        employee.setId(id);
        //assert
        assertEquals(expected,employee.getId());
    }

    @Test
    public void setIDNotEquals() {
        //arrange
        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker",23, "bilbo@gamil.com");
        Long id = new Long("2222222223");
        Long expected = new Long("2222222224");
        //act
        employee.setId(id);
        //assert
        assertNotEquals(expected,employee.getId());
    }

    @Test
    public void employeeNotNull() {
        //arrange

        //act
        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker",23, "bilbo@gamil.com");
        //assert
        assertNotNull(employee);
    }

    @Test
    public void employeeEmailEmpty() {
        //arrange
        Employee employee = new Employee("Bilbo", "Baggins", "Protector", "worker",23, "bilbo@gamil.com");
        String expected = "-";
        //act
        String email = employee.getEmail();
        //assert
        assertEquals(expected, email);
    }
}