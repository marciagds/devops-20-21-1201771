# Class Assignment 2 Report ( Part 1 ) #
___

    Márcia Guedes [1201771@isep.ipp.pt] | april 2021
___

### Build Tool ###

Build Tools are programs capable of executing the process of creating the application program for a software release. They take all the important source code files and compile them, which means turn source code into executable binary code. 

The Buil process can combine several steps, which vary according to each programming language and operating system. However, the basic concepts are universal.

Some of the common steps of the build process are:

    * Compile the code: it transforms source code into machine code. It can be handle by an interpreter (compilation happens simultaneously running process) or by a compiler (compilation happens before the running process). It is necessary to have a proper compiler with correct options installed.

    * Link object files and libraries: during the build process, it is necessary to include all the libraries and other kinds of services used in the source code. The library's version is a very important aspect controlled during the process.

    * Determine build order and dependencies: it is very important to track the dependencies. Even a small change (e.g. change library) can affect the build process and demand recompilation of the project. 

    * Test build results: it checks if the project doesn't contain bugs through the build tests. It assures that bugs get fixed and stay fixed, since, sometimes, bugs can creep back into code.

    * Package and/or Deployee: it allows for the installation of the program that was just compiled in a place of the system. This only happens if the build process happens successfully. In this final step, the project is put in executable mode, and all documentation, in a central location.

## Gradle as a Build Tool ##

Gradle is an open-source build tool able to build several different types of software. Its main features are:

    * High performance
    * JVM foundation
    * Conventions
    * Extensibility
    * IDE support
    * Insight

Any more information available at the following link: [https://docs.gradle.org/current/userguide/what_is_gradle.html].


#### Set up Graddle ####
If the user wants, it is possible to install Gradle software in the system. However, most systems already have compatibility with the Gradle Wrapper tool. To make sure that the user's system satisfies Gradle's prerequisites, the user should apply the following command. As long as the user has any JDK version in its path, Gradle Wrapper will work and Groovy  does not need to be installed.

        $ java -version
        java version "1.8.0_261"
        Java(TM) SE Runtime Environment (build 1.8.0_261-b12)
        Java HotSpot(TM) 64-Bit Server VM (build 25.261-b12, mixed mode)

When using the Gradle Wapper build tool the main difference is the command used.

        $ gradle <command>      (for Gradle)
        $ gradlew <command>     (for Gradle Wapper)


#### Set up .gitignore ####

One of the user's first steps should be to configure the .gitignore file. It is necessary to specify which files should be tracked and which ones shouldn't.  A possible configuration solution is through the gitignore.io (available on the following link: [https://www.toptal.com/developers/gitignore]. The user just needs to select Gradlew and add the result in the .gitignore file.

        (e.g.)
        # Created by https://www.toptal.com/developers/gitignore/api/gradle
        # Edit at https://www.toptal.com/developers/gitignore?templates=gradle

        ### Gradle ###
        .gradle
        build/

        # Ignore Gradle GUI config
        gradle-app.setting

        # Avoid ignoring Gradle wrapper jar file (.jar files are usually ignored)
        !gradle-wrapper.jar

        # Cache of project
        .gradletasknamecache

        # # Work around https://youtrack.jetbrains.com/issue/IDEA-116898
        # gradle/wrapper/gradle-wrapper.properties

        ### Gradle Patch ###
        **/build/

        # End of https://www.toptal.com/developers/gitignore/api/gradle


#### Work with Gradle ####

A demo application that implements a basic multithreaded chat room server will be used as an example.

##### Build the project #####
To build the source code, the user must run the following command. All the project properties and dependencies should be correctly added (e.g. java libraries). This command will create a .jar file with the application.

        $ gradlew build

        (e.g.)
        [...]\DevOps\devops-20-21-1201771\ca2\tut-gradle-p1\gradle_basic_demo> gradlew build
        > Task :compileJava UP-TO-DATE
        > Task :processResources UP-TO-DATE
        > Task :classes UP-TO-DATE
        > Task :jar UP-TO-DATE
        > Task :startScripts UP-TO-DATE
        > Task :distTar UP-TO-DATE
        > Task :distZip UP-TO-DATE
        > Task :assemble UP-TO-DATE
        > Task :compileTestJava NO-SOURCE
        > Task :processTestResources NO-SOURCE
        > Task :testClasses UP-TO-DATE
        > Task :test NO-SOURCE
        > Task :check UP-TO-DATE
        > Task :build UP-TO-DATE

        BUILD SUCCESSFUL in 2s  
        6 actionable tasks: 6 up-to-date


##### Executable Java #####

Initialize the program. Run executable java file. 

        [...]\DevOps\devops-20-21-1201771\ca2\tut-gradle-p1\gradle_basic_demo> java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
        The chat server is running...

 This Command Prompt must be intact. The process cannot be interrupted.

##### Run a task #####

On the project, Gradle is defined by one or more tasks. A task is a little part of the process during the build performance. Compiling a class is a typical example of a task. 

- Check available tasks

        $ gradlew tasks


- Run a task 

        $ gradlew <taskName>
        $ gradlew runClient             (e.g.)

###### Run a Client ######
The user must open a new Command Prompt and run the following task.

        [...]\DevOps\devops-20-21-1201771\ca2\tut-gradle-p1\gradle_basic_demo>gradlew runClient
        > Task :compileJava UP-TO-DATE
        > Task :processResources UP-TO-DATE
        > Task :classes UP-TO-DATE
        > Task :runClient

After running this task, a popup chatroom will appear, and the client should enter their name. (Note: This task must run a different command prompt.) 

![popups](image.PNG)

After running this task, the Command Prompt stated before shows now the users that login into the application. And if they stop the application.

        [...]\DevOps\devops-20-21-1201771\ca2\tut-gradle-p1\gradle_basic_demo>java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
        The chat server is running...
        15:18:38.592 [pool-1-thread-1] INFO  basic_demo.ChatServer.Handler - A new user has joined: maria
        15:19:56.936 [pool-1-thread-2] INFO  basic_demo.ChatServer.Handler - A new user has joined: john
        15:22:06.295 [pool-1-thread-1] INFO  basic_demo.ChatServer.Handler - maria has left the chat
        15:22:08.047 [pool-1-thread-2] INFO  basic_demo.ChatServer.Handler - john has left the chat


###### Run the Server ######
In the project, create a task to run the server, in the build.gradle file.

        //task to run server
        task runServer(type:JavaExec, dependsOn: classes){
                group = "DevOps"
                description = "Launches a chat server on port 59001 "

                lasspath = sourceSets.main.runtimeClasspath

                main = 'basic_demo.ChatServerApp'

                args '59001'
        }       


To run this task, the user must open the Command Prompt and run the task.

###### Backup task ######
Create a backup directory to save a copy of the source content files. Add the new task into the build.gradle file.

        //task to create a backup file of src content
        task copySrc(type: Copy) {
        group = "DevOps"

        description = "Copy src files to a backup package"

        from ('src/')
        into ('backup/')
        }

Run the task in the Command Prompt. The result will show a new directory in the project when run for the first time. After that, it will update this folder every time the user runs the task.

        $ gradlew copySrc
        > Task :copySrc

        BUILD SUCCESSFUL in 1s
        1 actionable task: 1 executed

###### Backup task ######
Create a zip file with the source content files. Add the new task into the build.gradle file.

        //task to create a zip folder of the src package
        task zipFilesSrc(type: Zip){

        group = "DevOps"

        description = "Zip src content to backup"

        from('src/')
        archiveName ('zipFile.zip')
        destinationDirectory = file('zipSrc')
        }

Run the task in the Command Prompt.

        $ gradlew zipFilesSrc
        > Task :zipFilesSrc

        BUILD SUCCESSFUL in 1s
        1 actionable task: 1 executed
        1 actionable task: 1 executed


#### Create a Test ####
When creating unit or integration tests, it may be necessary for the user to add some dependencies, concerning the JUnit. Therefore, it is necessary to add that dependency to the gradle.built file. After the dependency is added the test will run successfully.        


        dependencies {
        // Use Apache Log4J for logging
        implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
        implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
        implementation 'junit:junit:4.12'   //junit dependency
        } 

To run all tests of the project, the user must use the following command.

        $ gradlew clean test
        
        (e.g.)
        [...]\DevOps\devops-20-21-1201771\ca2\tut-gradle-p1\gradle_basic_demo>gradlew clean test
        If the user > Task :clean
        > Task :compileJava
        > Task :processResources
        > Task :classes
        > Task :compileTestJava
        > Task :processTestResources NO-SOURCE
        > Task :testClasses
        > Task :test

        BUILD SUCCESSFUL in 3s
        5 actionable tasks: 5 executed        

If the user wants a report of the tests, their should use the following command.

        $ gradlew clean test --scan

        (e.g.)
        [...]\DevOps\devops-20-21-1201771\ca2\tut-gradle-p1\gradle_basic_demo>gradlew clean test --scan
        > Task :clean
        > Task :compileJava
        > Task :processResources
        > Task :classes
        > Task :compileTestJava
        > Task :processTestResources NO-SOURCE
        > Task :testClasses
        > Task :test

        BUILD SUCCESSFUL in 7s
        5 actionable tasks: 5 executed

        Publishing a build scan to scans.gradle.com requires accepting the Gradle Terms of Service defined at https://gradle.com/terms-of-service. Do you accept these terms? [yes, no] yes

        Gradle Terms of Service accepted.

        Publishing build scan...
        https://gradle.com/s/ry4zacm3nfe3c

When the user accesses the link, it should appear as a report like demonstrated above. In the first attempt, it will, probably, be required for the user to register in the service. After that, the user only needs to go to the Tests Directory.

![report](report.PNG)